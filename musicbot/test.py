import os
import json
import io
import operator

def updateJson(obj):
    with io.open('C:/Users/Juven/MusicBot/ranks.txt', 'w+',encoding="utf-8-sig") as f:
        f.write(json.dumps(obj,indent = 4, sort_keys = True, ensure_ascii=False))

def getRanks():
    ret = ""
    with io.open('C:/Users/Juven/MusicBot/ranks.txt', 'r+',encoding="utf-8-sig") as f:
        jsonData = json.load(f)
        data = jsonData['ranks']
        for x in sorted(data, key = lambda k: k['Score'], reverse=True):
            ret += '**' + x['Username'] + '** --> ' + str(x['Score']) + '\n'
        return ret

def readJson():
    with io.open('C:/Users/Juven/MusicBot/ranks.txt', 'r+',encoding="utf-8-sig") as f:
        return json.load(f)
    
def createData(name, score):
    return {'Username' : name, 'Score' : score}

def addUser(name, score):
    jsonFile = readJson()
    data = jsonFile['ranks']
    
    for x in data:
        if(x['Username'] == name):
            print('{} already exists!'.format(name))
            return False
    data.append(createData(name, score))
    print('{} added to ranks!'.format(name))
    updateJson(jsonFile)
    return True

def addScore(name):
    jsonFile = readJson()
    data = jsonFile['ranks']
    found = False
    for x in data:
        if(x['Username'] == name):
            x['Score'] = x['Score'] + 1
            print('{} score updated'.format(name))
            updateJson(jsonFile)
            return True
    print('User not found')
    return False
