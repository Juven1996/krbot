import json
import urllib.request
from bs4 import BeautifulSoup
from datetime import timezone
from datetime import datetime
from dateutil import tz
import io
import re

def pretty_time_delta(seconds):
    sign_string = '-' if seconds < 0 else ''
    seconds = abs(int(seconds))
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%s%dd %dh %dm %ds' % (sign_string, days, hours, minutes, seconds)
    elif hours > 0:
        return '%s%dh %dm %ds' % (sign_string, hours, minutes, seconds)
    elif minutes > 0:
        return '%s%dm %ds' % (sign_string, minutes, seconds)
    else:
        return '%s%ds' % (sign_string, seconds)
        
def readJson():
    with io.open('C:/Users/Juven/MusicBot/SolNodes.json', 'r+') as f:
        return json.load(f)

def getRes():
    with urllib.request.urlopen('http://content.warframe.com/dynamic/worldState.php') as response:
            return response.read()

def getTime(val):
    return datetime.utcfromtimestamp(val).replace(tzinfo=timezone.utc).astimezone(tz=None)

def getAlerts():
    strRes = '**Alerts**\n```'
    nodes = readJson()
    data = json.loads(getRes().decode('utf-8'))
    alerts = data['Alerts']
    for x in alerts:
        f = float(x['Expiry']['$date']['$numberLong'])/1000
        strRes += 'Location : {}\n'.format(nodes[x['MissionInfo']['location']]['value'])
        strRes += 'Time Left: {}\n'.format(pretty_time_delta((getTime(f) - datetime.utcnow().replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None)).seconds))
        strRes += 'Credits  : {}\n'.format(x['MissionInfo']['missionReward']['credits'])
        if 'items' in x['MissionInfo']['missionReward']:
            items = []
            for i in x['MissionInfo']['missionReward']['items']:
                temp = i.split('/')
                if 'fusion' in temp[len(temp)-1].lower():
                    if 'small' in temp[len(temp)-1].lower():
                        items.append('80 ENDO')
                    elif 'medium' in temp[len(temp)-1].lower():
                        items.append('100 ENDO')
                    elif 'large' in temp[len(temp)-1].lower():
                        items.append('150 ENDO')
                    continue
                if 'alertium' in temp[len(temp)-1].lower():
                    items.append('Nitain Extract')
                    continue
                if 'trapper' in temp[len(temp)-1].lower():
                    temp[len(temp)-1] = temp[len(temp)-1].replace('Trapper','Vauban')
                if 'pirate' in temp[len(temp)-1].lower():
                    temp[len(temp)-1] = temp[len(temp)-1].replace('Pirate','Hydroid')    
                if 'sandman' in temp[len(temp)-1].lower():
                    temp[len(temp)-1] = temp[len(temp)-1].replace('Sandman','Inaros')    
                items.append(ccs(temp[len(temp)-1]))
            strRes += ('Items    : {}\n'.format(' '.join(items)))
        if 'countedItems' in x['MissionInfo']['missionReward']:
            reslist = []
            for res in x['MissionInfo']['missionReward']['countedItems']:
                resType = res['ItemType'].split('/')
                resource = resType[len(resType)-1]
                if resource.lower() == 'alertium':
                    reslist.append('{} Nitain Extract'.format(res['ItemCount']))
                elif resource.lower() == 'eventium':
                    reslist.append('{} Synthula'.format(res['ItemCount']))
                elif resource.lower() == 'biocomponent':
                    reslist.append('{} Mutagen Mass'.format(res['ItemCount']))
                elif resource.lower() == 'energycomponent':
                    reslist.append('{} Fieldron'.format(res['ItemCount']))
                elif resource.lower() == 'chemcomponent':
                    reslist.append('{} Detonite Injector'.format(res['ItemCount']))
                else:
                    reslist.append('{} {}'.format(res['ItemCount'], ccs(resource)))
            strRes += ('Resources: {}\n'.format(' '.join(reslist)))
        strRes += '\n'
    strRes += '```'
    return strRes

def ccs(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1 \2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1 \2', s1)
    
def getInvasions():
    strRes = '**Invasions**\n```'
    nodes = readJson()
    data = json.loads(getRes().decode('utf-8'))
    invasions = [x for x in data['Invasions'] if x['Completed'] == False]
    for x in invasions:
        atkresources = []
        atkitems = []
        defresources = []
        defitems = []
        if 'countedItems' in x['AttackerReward']:
            for res in x['AttackerReward']['countedItems']:
                resType = res['ItemType'].split('/')
                resource = resType[len(resType)-1]
                if resource.lower() == 'alertium':
                    atkresources.append('{} Nitain Extract'.format(res['ItemCount']))
                elif resource.lower() == 'eventium':
                    atkresources.append('{} Synthula'.format(res['ItemCount']))
                elif resource.lower() == 'biocomponent':
                    atkresources.append('{} Mutagen Mass'.format(res['ItemCount']))
                elif resource.lower() == 'energycomponent':
                    atkresources.append('{} Fieldron'.format(res['ItemCount']))
                elif resource.lower() == 'chemcomponent':
                    atkresources.append('{} Detonite Injector'.format(res['ItemCount']))                    
                else:
                    atkresources.append('{} {}'.format(res['ItemCount'], ccs(resource)))
        if 'items' in x['AttackerReward']:
            for i in x['AttackerReward']['items']:
                temp = i.split('/')
                if 'fusion' in temp[len(temp)-1].lower():
                    if 'small' in temp[len(temp)-1].lower():
                        atkitems.append('80 ENDO')
                    elif 'medium' in temp[len(temp)-1].lower():
                        atkitems.append('100 ENDO')
                    elif 'large' in temp[len(temp)-1].lower():
                        atkitems.append('150 ENDO')
                    continue
                atkitems.append(ccs(temp[len(temp)-1]))
        if 'countedItems' in x['DefenderReward']:
            for res in x['DefenderReward']['countedItems']:
                resType = res['ItemType'].split('/')
                resource = resType[len(resType)-1]
                if resource.lower() == 'alertium':
                    defresources.append('{} Nitain Extract'.format(res['ItemCount']))
                elif resource.lower() == 'eventium':
                    defresources.append('{} Synthula'.format(res['ItemCount']))
                elif resource.lower() == 'biocomponent':
                    defresources.append('{} Mutagen Mass'.format(res['ItemCount']))
                elif resource.lower() == 'energycomponent':
                    defresources.append('{} Fieldron'.format(res['ItemCount']))
                elif resource.lower() == 'chemcomponent':
                    defresources.append('{} Detonite Injector'.format(res['ItemCount']))     
                else:
                    defresources.append('{} {}'.format(res['ItemCount'], ccs(resource)))
        if 'items' in x['DefenderReward']:
            for i in x['DefenderReward']['items']:
                temp = i.split('/')
                if 'fusion' in temp[len(temp)-1].lower():
                    if 'small' in temp[len(temp)-1].lower():
                        defitems.append('80 ENDO')
                    elif 'medium' in temp[len(temp)-1].lower():
                        defitems.append('100 ENDO')
                    elif 'large' in temp[len(temp)-1].lower():
                        defitems.append('150 ENDO')
                    continue
                defitems.append(ccs(temp[len(temp)-1]))
        f = float(x['Activation']['$date']['$numberLong'])/1000
        strRes += 'Location           : {}\n'.format(nodes[x['Node']]['value'])
        if x['Count'] < 0:
            strRes += 'Progress           : {}\n'.format('{:.2f}% on Defender'.format((abs(x['Goal'] - abs(x['Count'])) / x['Goal'] * 100)))
        else:
            strRes += 'Progress           : {}\n'.format('{:.2f}% on Attacker'.format((abs(x['Goal'] - abs(x['Count'])) / x['Goal'] * 100)))
        if atkresources:
            strRes += 'Attacker Resources : {}\n'.format(' '.join(atkresources))
        if atkitems:
            strRes += 'Attacker Items     : {}\n'.format(' '.join(atkitems))
        if defresources:
            strRes += 'Defender Resources : {}\n'.format(' '.join(defresources))
        if defitems:
            strRes += 'Defender Items     : {}\n'.format(' '.join(defitems))
        strRes += 'Activation time    : {}\n'.format(pretty_time_delta((getTime(f) - datetime.utcnow().replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None)).seconds))
        strRes += '\n'
    strRes += '```'
    return strRes
