﻿import os
import sys
import time
import shlex
import shutil
import inspect
import aiohttp
import discord
import asyncio
import traceback
import json
import io
import giphypop as gp
import urllib.request
import random
import subprocess
import re
import difflib 
import psutil as ps
import uptime as ut

from selenium import webdriver

from apscheduler.schedulers.asyncio import AsyncIOScheduler as bs

from urllib.request import Request, urlopen

from datetime import datetime
from datetime import timezone
from dateutil import tz

from discord import utils
from discord.object import Object
from discord.enums import ChannelType
from discord.voice_client import VoiceClient
from discord.ext.commands.bot import _get_variable

from io import BytesIO
from functools import wraps
from textwrap import dedent
from datetime import timedelta
from random import choice, shuffle
from collections import defaultdict

from bs4 import BeautifulSoup

from musicbot.playlist import Playlist
from musicbot.player import MusicPlayer
from musicbot.config import Config, ConfigDefaults
from musicbot.permissions import Permissions, PermissionsDefaults
from musicbot.utils import load_file, write_file, sane_round_int

from . import exceptions
from . import downloader
from . import test
from . import wfJSON as wf
from .opus_loader import load_opus_lib
from .constants import VERSION as BOTVERSION
from .constants import DISCORD_MSG_CHAR_LIMIT, AUDIO_CACHE_PATH

from riotwatcher import RiotWatcher

from collections import deque

load_opus_lib()


class SkipState:
    def __init__(self):
        self.skippers = set()
        self.skip_msgs = set()

    @property
    def skip_count(self):
        return len(self.skippers)

    def reset(self):
        self.skippers.clear()
        self.skip_msgs.clear()

    def add_skipper(self, skipper, msg):
        self.skippers.add(skipper)
        self.skip_msgs.add(msg)
        return self.skip_count


class Response:
    def __init__(self, content, reply=False, delete_after=0):
        self.content = content
        self.reply = reply
        self.delete_after = delete_after


class MusicBot(discord.Client):
    tracking = False
    tracked_item = ''
    sched = None
    interval = 30
    banned = deque()
    chat_enabled = False
    muted = deque()
    
    def __init__(self, config_file=ConfigDefaults.options_file, perms_file=PermissionsDefaults.perms_file):
        self.players = {}
        self.the_voice_clients = {}
        self.locks = defaultdict(asyncio.Lock)
        self.voice_client_connect_lock = asyncio.Lock()
        self.voice_client_move_lock = asyncio.Lock()

        self.config = Config(config_file)
        self.permissions = Permissions(perms_file, grant_all=[self.config.owner_id])

        self.blacklist = set(load_file(self.config.blacklist_file))
        self.autoplaylist = load_file(self.config.auto_playlist_file)
        self.downloader = downloader.Downloader(download_folder='audio_cache')
        self.song_list = self.autoplaylist[:]
        self.exit_signal = None
        self.init_ok = False
        self.cached_client_id = None

        if not self.autoplaylist:
            print("Warning: Autoplaylist is empty, disabling.")
            self.config.auto_playlist = False

        ssd_defaults = {'last_np_msg': None, 'auto_paused': False}
        self.server_specific_data = defaultdict(lambda: dict(ssd_defaults))

        super().__init__()
        self.aiosession = aiohttp.ClientSession(loop=self.loop)
        self.http.user_agent += ' MusicBot/%s' % BOTVERSION
        self.banned = [id for id in open('config/banlist.txt','r',encoding="utf8").readlines() if id.strip()]
        
    def owner_only(func):
        @wraps(func)
        async def wrapper(self, *args, **kwargs):

            orig_msg = _get_variable('message')

            if not orig_msg or orig_msg.author.id == self.config.owner_id:
                return await func(self, *args, **kwargs)
            else:
                raise exceptions.PermissionsError("only the owner can use this command", expire_in=30)

        return wrapper

    @staticmethod
    def _fixg(x, dp=2):
        return ('{:.%sf}' % dp).format(x).rstrip('0').rstrip('.')

    def _get_owner(self, voice=False):
        if voice:
            for server in self.servers:
                for channel in server.channels:
                    for m in channel.voice_members:
                        if m.id == self.config.owner_id:
                            return m
        else:
            return discord.utils.find(lambda m: m.id == self.config.owner_id, self.get_all_members())

    def _delete_old_audiocache(self, path=AUDIO_CACHE_PATH):
        try:
            shutil.rmtree(path)
            return True
        except:
            try:
                os.rename(path, path + '__')
            except:
                return False
            try:
                shutil.rmtree(path)
            except:
                os.rename(path + '__', path)
                return False

        return True

    async def _auto_summon(self):
        owner = self._get_owner(voice=True)
        if owner:
            self.safe_print("Found owner in \"%s\", attempting to join..." % owner.voice_channel.name)

            await self.cmd_summon(owner.voice_channel, owner, None)
            return owner.voice_channel

    async def _autojoin_channels(self, channels):
        joined_servers = []

        for channel in channels:
            if channel.server in joined_servers:
                print("Already joined a channel in %s, skipping" % channel.server.name)
                continuef

            if channel and channel.type == discord.ChannelType.voice:
                self.safe_print("Attempting to autojoin %s in %s" % (channel.name, channel.server.name))

                chperms = channel.permissions_for(channel.server.me)

                if not chperms.connect:
                    self.safe_print("Cannot join channel \"%s\", no permission." % channel.name)
                    continue

                elif not chperms.speak:
                    self.safe_print("Will not join channel \"%s\", no permission to speak." % channel.name)
                    continue

                try:
                    player = await self.get_player(channel, create=True)

                    if player.is_stopped:
                        player.play()
                    if self.config.auto_playlist:
                        await self.on_player_finished_playing(player)

                    joined_servers.append(channel.server)
                except Exception as e:
                    if self.config.debug_mode:
                        traceback.print_exc()
                    print("Failed to join", channel.name)

            elif channel:
                print("Not joining %s on %s, that's a text channel." % (channel.name, channel.server.name))

            else:
                print("Invalid channel thing: " + channel)

    async def _wait_delete_msg(self, message, after):
        await asyncio.sleep(after)
        await self.safe_delete_message(message)

    async def _manual_delete_check(self, message, *, quiet=False):
        if self.config.delete_invoking:
            await self.safe_delete_message(message, quiet=quiet)

    async def _check_ignore_non_voice(self, msg):
        vc = msg.server.me.voice_channel

        if not vc or vc == msg.author.voice_channel:
            return True
        else:
            raise exceptions.PermissionsError(
                "you cannot use this command when not in the voice channel (%s)" % vc.name, expire_in=30)

    async def generate_invite_link(self, *, permissions=None, server=None):
        if not self.cached_client_id:
            appinfo = await self.application_info()
            self.cached_client_id = appinfo.id

        return discord.utils.oauth_url(self.cached_client_id, permissions=permissions, server=server)

    async def get_voice_client(self, channel):
        if isinstance(channel, Object):
            channel = self.get_channel(channel.id)

        if getattr(channel, 'type', ChannelType.text) != ChannelType.voice:
            raise AttributeError('Channel passed must be a voice channel')

        with await self.voice_client_connect_lock:
            server = channel.server
            if server.id in self.the_voice_clients:
                return self.the_voice_clients[server.id]

            s_id = self.ws.wait_for('VOICE_STATE_UPDATE', lambda d: d.get('user_id') == self.user.id)
            _voice_data = self.ws.wait_for('VOICE_SERVER_UPDATE', lambda d: True)

            await self.ws.voice_state(server.id, channel.id)

            s_id_data = await asyncio.wait_for(s_id, timeout=10, loop=self.loop)
            voice_data = await asyncio.wait_for(_voice_data, timeout=10, loop=self.loop)
            session_id = s_id_data.get('session_id')

            kwargs = {
                'user': self.user,
                'channel': channel,
                'data': voice_data,
                'loop': self.loop,
                'session_id': session_id,
                'main_ws': self.ws
            }
            voice_client = VoiceClient(**kwargs)
            self.the_voice_clients[server.id] = voice_client

            retries = 3
            for x in range(retries):
                try:
                    print("Attempting connection...")
                    await asyncio.wait_for(voice_client.connect(), timeout=10, loop=self.loop)
                    print("Connection established.")
                    break
                except:
                    traceback.print_exc()
                    print("Failed to connect, retrying (%s/%s)..." % (x+1, retries))
                    await asyncio.sleep(1)
                    await self.ws.voice_state(server.id, None, self_mute=True)
                    await asyncio.sleep(1)

                    if x == retries-1:
                        raise exceptions.HelpfulError(
                            "Cannot establish connection to voice chat.  "
                            "Something may be blocking outgoing UDP connections.",

                            "This may be an issue with a firewall blocking UDP.  "
                            "Figure out what is blocking UDP and disable it.  "
                            "It's most likely a system firewall or overbearing anti-virus firewall.  "
                        )

            return voice_client

    async def mute_voice_client(self, channel, mute):
        await self._update_voice_state(channel, mute=mute)

    async def deafen_voice_client(self, channel, deaf):
        await self._update_voice_state(channel, deaf=deaf)

    async def move_voice_client(self, channel):
        await self._update_voice_state(channel)

    async def reconnect_voice_client(self, server):
        if server.id not in self.the_voice_clients:
            return

        vc = self.the_voice_clients.pop(server.id)
        _paused = False

        player = None
        if server.id in self.players:
            player = self.players[server.id]
            if player.is_playing:
                player.pause()
                _paused = True

        try:
            await vc.disconnect()
        except:
            print("Error disconnecting during reconnect")
            traceback.print_exc()

        await asyncio.sleep(0.1)

        if player:
            new_vc = await self.get_voice_client(vc.channel)
            player.reload_voice(new_vc)

            if player.is_paused and _paused:
                player.resume()

    async def disconnect_voice_client(self, server):
        if server.id not in self.the_voice_clients:
            return

        if server.id in self.players:
            self.players.pop(server.id).kill()

        await self.the_voice_clients.pop(server.id).disconnect()

    async def disconnect_all_voice_clients(self):
        for vc in self.the_voice_clients.copy().values():
            await self.disconnect_voice_client(vc.channel.server)

    async def _update_voice_state(self, channel, *, mute=False, deaf=False):
        if isinstance(channel, Object):
            channel = self.get_channel(channel.id)

        if getattr(channel, 'type', ChannelType.text) != ChannelType.voice:
            raise AttributeError('Channel passed must be a voice channel')

        with await self.voice_client_move_lock:
            server = channel.server

            payload = {
                'op': 4,
                'd': {
                    'guild_id': server.id,
                    'channel_id': channel.id,
                    'self_mute': mute,
                    'self_deaf': deaf
                }
            }

            await self.ws.send(utils.to_json(payload))
            self.the_voice_clients[server.id].channel = channel

    async def get_player(self, channel, create=False) -> MusicPlayer:
        server = channel.server

        if server.id not in self.players:
            if not create:
                raise exceptions.CommandError(
                    'The bot is not in a voice channel.  '
                    'Use %ssummon to summon it to your voice channel.' % self.config.command_prefix)

            voice_client = await self.get_voice_client(channel)

            playlist = Playlist(self)
            player = MusicPlayer(self, voice_client, playlist) \
                .on('play', self.on_player_play) \
                .on('resume', self.on_player_resume) \
                .on('pause', self.on_player_pause) \
                .on('stop', self.on_player_stop) \
                .on('finished-playing', self.on_player_finished_playing) \
                .on('entry-added', self.on_player_entry_added)

            player.skip_state = SkipState()
            self.players[server.id] = player

        return self.players[server.id]

    async def on_player_play(self, player, entry):
        await self.update_now_playing(entry)
        player.skip_state.reset()

        channel = entry.meta.get('channel', None)
        author = entry.meta.get('author', None)

        if channel and author:
            last_np_msg = self.server_specific_data[channel.server]['last_np_msg']
            if last_np_msg and last_np_msg.channel == channel:

                async for lmsg in self.logs_from(channel, limit=1):
                    if lmsg != last_np_msg and last_np_msg:
                        await self.safe_delete_message(last_np_msg)
                        self.server_specific_data[channel.server]['last_np_msg'] = None
                    break  

            if self.config.now_playing_mentions:
                newmsg = '%s - your song **%s** is now playing in %s!' % (
                    entry.meta['author'].mention, entry.title, player.voice_client.channel.name)
            else:
                newmsg = 'Now playing in %s: **%s**' % (
                    player.voice_client.channel.name, entry.title)

            if self.server_specific_data[channel.server]['last_np_msg']:
                self.server_specific_data[channel.server]['last_np_msg'] = await self.safe_edit_message(last_np_msg, newmsg, send_if_fail=True)
            else:
                self.server_specific_data[channel.server]['last_np_msg'] = await self.safe_send_message(channel, newmsg)

    async def on_player_resume(self, entry, **_):
        await self.update_now_playing(entry)

    async def on_player_pause(self, entry, **_):
        await self.update_now_playing(entry, True)

    async def on_player_stop(self, **_):
        await self.update_now_playing()

    async def on_player_finished_playing(self, player, **_):
        if not player.playlist.entries and not player.current_entry and self.config.auto_playlist:
            while self.autoplaylist:
                random.shuffle(self.song_list)
                if (len(self.song_list) == 0):
                    print('All songs have been played. Restarting auto playlist...')
                    self.song_list = self.autoplaylist[:]
                song_url = random.choice(self.song_list)
                self.song_list.remove(song_url)
                info = await self.downloader.safe_extract_info(player.playlist.loop, song_url, download=False, process=False)

                if not info:
                    self.autoplaylist.remove(song_url)
                    self.safe_print("[Info] Removing unplayable song from autoplaylist: %s" % song_url)
                    write_file(self.config.auto_playlist_file, self.autoplaylist)
                    continue

                if info.get('entries', None):  
                    pass  

                try:
                    await player.playlist.add_entry(song_url, channel=None, author=None)
                except exceptions.ExtractionError as e:
                    print("Error adding song from autoplaylist:", e)
                    continue

                break

            if not self.autoplaylist:
                print("[Warning] No playable songs in the autoplaylist, disabling.")
                self.config.auto_playlist = False

    async def on_player_entry_added(self, playlist, entry, **_):
        pass

    async def update_now_playing(self, entry=None, is_paused=False):
        game = None

        if self.user.bot:
            activeplayers = sum(1 for p in self.players.values() if p.is_playing)
            if activeplayers > 1:
                game = discord.Game(name="music on %s servers" % activeplayers)
                entry = None

            elif activeplayers == 1:
                player = discord.utils.get(self.players.values(), is_playing=True)
                entry = player.current_entry

        if entry:
            prefix = u'\u275A\u275A ' if is_paused else ''

            name = u'{}{}'.format(prefix, entry.title)[:128]
            game = discord.Game(name=name)

        await self.change_status(game)


    async def safe_send_message(self, dest, content, *, tts=False, expire_in=0, also_delete=None, quiet=False):
        msg = None
        try:
            msg = await self.send_message(dest, content, tts=tts)

            if msg and expire_in:
                asyncio.ensure_future(self._wait_delete_msg(msg, expire_in))

            if also_delete and isinstance(also_delete, discord.Message):
                asyncio.ensure_future(self._wait_delete_msg(also_delete, expire_in))

        except discord.Forbidden:
            if not quiet:
                self.safe_print("Warning: Cannot send message to %s, no permission" % dest.name)

        except discord.NotFound:
            if not quiet:
                self.safe_print("Warning: Cannot send message to %s, invalid channel?" % dest.name)

        return msg

    async def safe_delete_message(self, message, *, quiet=False):
        try:
            return await self.delete_message(message)

        except discord.Forbidden:
            if not quiet:
                self.safe_print("Warning: Cannot delete message \"%s\", no permission" % message.clean_content)

        except discord.NotFound:
            if not quiet:
                self.safe_print("Warning: Cannot delete message \"%s\", message not found" % message.clean_content)

    async def safe_edit_message(self, message, new, *, send_if_fail=False, quiet=False):
        try:
            return await self.edit_message(message, new)

        except discord.NotFound:
            if not quiet:
                self.safe_print("Warning: Cannot edit message \"%s\", message not found" % message.clean_content)
            if send_if_fail:
                if not quiet:
                    print("Sending instead")
                return await self.safe_send_message(message.channel, new)

    def safe_print(self, content, *, end='\n', flush=True):
        sys.stdout.buffer.write((content + end).encode('utf-8', 'replace'))
        if flush: sys.stdout.flush()

    async def send_typing(self, destination):
        try:
            return await super().send_typing(destination)
        except discord.Forbidden:
            if self.config.debug_mode:
                print("Could not send typing to %s, no permssion" % destination)

    async def edit_profile(self, **fields):
        if self.user.bot:
            return await super().edit_profile(**fields)
        else:
            return await super().edit_profile(self.config._password,**fields)

    def _cleanup(self):
        try:
            self.loop.run_until_complete(self.logout())
        except: 
            pass

        pending = asyncio.Task.all_tasks()
        gathered = asyncio.gather(*pending)

        try:
            gathered.cancel()
            self.loop.run_until_complete(gathered)
            gathered.exception()
        except: 
            pass

    def run(self):
        try:
            self.loop.run_until_complete(self.start(*self.config.auth))

        except discord.errors.LoginFailure:

            raise exceptions.HelpfulError(
                "Bot cannot login, bad credentials.",
                "Fix your Email or Password or Token in the options file.  "
                "Remember that each field should be on their own line.")

        finally:
            try:
                self._cleanup()
            except Exception as e:
                print("Error in cleanup:", e)

            self.loop.close()
            if self.exit_signal:
                raise self.exit_signal

    async def logout(self):
        await self.disconnect_all_voice_clients()
        return await super().logout()

    async def on_error(self, event, *args, **kwargs):
        ex_type, ex, stack = sys.exc_info()

        if ex_type == exceptions.HelpfulError:
            print("Exception in", event)
            print(ex.message)

            await asyncio.sleep(2)  
            await self.logout()

        elif issubclass(ex_type, exceptions.Signal):
            self.exit_signal = ex_type
            await self.logout()

        else:
            traceback.print_exc()

    async def on_resumed(self):
        for vc in self.the_voice_clients.values():
            vc.main_ws = self.ws
            
    async def on_ready(self):
        print('\rConnected!  Musicbot v%s\n' % BOTVERSION)

        if self.config.owner_id == self.user.id:
            raise exceptions.HelpfulError(
                "Your OwnerID is incorrect or you've used the wrong credentials.",

                "The bot needs its own account to function.  "
                "The OwnerID is the id of the owner, not the bot.  "
                "Figure out which one is which and use the correct information.")

        self.init_ok = True

        self.safe_print("Bot:   %s/%s#%s" % (self.user.id, self.user.name, self.user.discriminator))

        owner = self._get_owner(voice=True) or self._get_owner()
        if owner and self.servers:
            self.safe_print("Owner: %s/%s#%s\n" % (owner.id, owner.name, owner.discriminator))

            print('Server List:')
            [self.safe_print(' - ' + s.name) for s in self.servers]

        elif self.servers:
            print("Owner could not be found on any server (id: %s)\n" % self.config.owner_id)

            print('Server List:')
            [self.safe_print(' - ' + s.name) for s in self.servers]

        else:
            print("Owner unknown, bot is not on any servers.")
            if self.user.bot:
                print("\nTo make the bot join a server, paste this link in your browser.")
                print("Note: You should be logged into your main account and have \n"
                      "manage server permissions on the server you want the bot to join.\n")
                print("    " + await self.generate_invite_link())

        print()

        if self.config.bound_channels:
            chlist = set(self.get_channel(i) for i in self.config.bound_channels if i)
            chlist.discard(None)
            invalids = set()

            invalids.update(c for c in chlist if c.type == discord.ChannelType.voice)
            chlist.difference_update(invalids)
            self.config.bound_channels.difference_update(invalids)

            print("Bound to text channels:")
            [self.safe_print(' - %s/%s' % (ch.server.name.strip(), ch.name.strip())) for ch in chlist if ch]

            if invalids and self.config.debug_mode:
                print("\nNot binding to voice channels:")
                [self.safe_print(' - %s/%s' % (ch.server.name.strip(), ch.name.strip())) for ch in invalids if ch]

            print()

        else:
            print("Not bound to any text channels")

        if self.config.autojoin_channels:
            chlist = set(self.get_channel(i) for i in self.config.autojoin_channels if i)
            chlist.discard(None)
            invalids = set()

            invalids.update(c for c in chlist if c.type == discord.ChannelType.text)
            chlist.difference_update(invalids)
            self.config.autojoin_channels.difference_update(invalids)

            print("Autojoining voice chanels:")
            [self.safe_print(' - %s/%s' % (ch.server.name.strip(), ch.name.strip())) for ch in chlist if ch]

            if invalids and self.config.debug_mode:
                print("\nCannot join text channels:")
                [self.safe_print(' - %s/%s' % (ch.server.name.strip(), ch.name.strip())) for ch in invalids if ch]

            autojoin_channels = chlist

        else:
            print("Not autojoining any voice channels")
            autojoin_channels = set()

        print()
        print("Options:")

        self.safe_print("  Command prefix: " + self.config.command_prefix)
        print("  Default volume: %s%%" % int(self.config.default_volume * 100))
        print("  Skip threshold: %s votes or %s%%" % (
            self.config.skips_required, self._fixg(self.config.skip_ratio_required * 100)))
        print("  Now Playing @mentions: " + ['Disabled', 'Enabled'][self.config.now_playing_mentions])
        print("  Auto-Summon: " + ['Disabled', 'Enabled'][self.config.auto_summon])
        print("  Auto-Playlist: " + ['Disabled', 'Enabled'][self.config.auto_playlist])
        print("  Auto-Pause: " + ['Disabled', 'Enabled'][self.config.auto_pause])
        print("  Delete Messages: " + ['Disabled', 'Enabled'][self.config.delete_messages])
        if self.config.delete_messages:
            print("    Delete Invoking: " + ['Disabled', 'Enabled'][self.config.delete_invoking])
        print("  Debug Mode: " + ['Disabled', 'Enabled'][self.config.debug_mode])
        print("  Downloaded songs will be %s" % ['deleted', 'saved'][self.config.save_videos])
        print()


        if not self.config.save_videos and os.path.isdir(AUDIO_CACHE_PATH):
            if self._delete_old_audiocache():
                print("Deleting old audio cache")
            else:
                print("Could not delete old audio cache, moving on.")

        if self.config.autojoin_channels:
            await self._autojoin_channels(autojoin_channels)

        elif self.config.auto_summon:
            print("Attempting to autosummon...", flush=True)

            owner_vc = await self._auto_summon()

            if owner_vc:
                print("Done!", flush=True)  
                if self.config.auto_playlist:
                    print("Starting auto-playlist")
                    await self.on_player_finished_playing(await self.get_player(owner_vc))
            else:
                print("Owner not found in a voice channel, could not autosummon.")

        print()


    async def cmd_help(self, command=None):
        """
        Usage:
            {command_prefix}help [command]

        Prints a help message.
        If a command is specified, it prints a help message for that command.
        Otherwise, it lists the available commands.
        """

        if command:
            cmd = getattr(self, 'cmd_' + command, None)
            if cmd:
                return Response(
                    "```\n{}```".format(
                        dedent(cmd.__doc__),
                        command_prefix=self.config.command_prefix
                    ),
                    delete_after=60
                )
            else:
                return Response("No such command", delete_after=10)

        else:
            helpmsg = "**Commands**\n```"
            commands = []

            for att in dir(self):
                if att.startswith('cmd_') and att != 'cmd_help':
                    command_name = att.replace('cmd_', '').lower()
                    commands.append("{}{}".format(self.config.command_prefix, command_name))

            helpmsg += ", ".join(commands)
            helpmsg += "```"
            

            return Response(helpmsg, reply=True, delete_after=60)

    async def cmd_blacklist(self, message, user_mentions, option, something):
        """
        Usage:
            {command_prefix}blacklist [ + | - | add | remove ] @UserName [@UserName2 ...]

        Add or remove users to the blacklist.
        Blacklisted users are forbidden from using bot commands.
        """

        if not user_mentions:
            raise exceptions.CommandError("No users listed.", expire_in=20)

        if option not in ['+', '-', 'add', 'remove']:
            raise exceptions.CommandError(
                'Invalid option "%s" specified, use +, -, add, or remove' % option, expire_in=20
            )

        for user in user_mentions.copy():
            if user.id == self.config.owner_id:
                print("[Commands:Blacklist] The owner cannot be blacklisted.")
                user_mentions.remove(user)

        old_len = len(self.blacklist)

        if option in ['+', 'add']:
            self.blacklist.update(user.id for user in user_mentions)

            write_file(self.config.blacklist_file, self.blacklist)

            return Response(
                '%s users have been added to the blacklist' % (len(self.blacklist) - old_len),
                reply=True, delete_after=10
            )

        else:
            if self.blacklist.isdisjoint(user.id for user in user_mentions):
                return Response('none of those users are in the blacklist.', reply=True, delete_after=10)

            else:
                self.blacklist.difference_update(user.id for user in user_mentions)
                write_file(self.config.blacklist_file, self.blacklist)

                return Response(
                    '%s users have been removed from the blacklist' % (old_len - len(self.blacklist)),
                    reply=True, delete_after=10
                )

    async def cmd_id(self, author, user_mentions):
        """
        Usage:
            {command_prefix}id [@user]

        Tells the user their id or the id of another user.
        """
        if not user_mentions:
            return Response('your id is `%s`' % author.id, reply=True, delete_after=35)
        else:
            usr = user_mentions[0]
            return Response("%s's id is `%s`" % (usr.name, usr.id), reply=True, delete_after=35)

    @owner_only
    async def cmd_joinserver(self, message, server_link=None):
        """
        Usage:
            {command_prefix}joinserver invite_link

        Asks the bot to join a server.  Note: Bot accounts cannot use invite links.
        """

        if self.user.bot:
            url = await self.generate_invite_link()
            return Response(
                "Bot accounts can't use invite links!  Click here to invite me: \n{}".format(url),
                reply=True, delete_after=30
            )

        try:
            if server_link:
                await self.accept_invite(server_link)
                return Response(":+1:")

        except:
            raise exceptions.CommandError('Invalid URL provided:\n{}\n'.format(server_link), expire_in=30)

    async def cmd_play(self, player, channel, author, permissions, leftover_args, song_url):
        """
        Usage:
            {command_prefix}play song_link
            {command_prefix}play text to search for

        Adds the song to the playlist.  If a link is not provided, the first
        result from a youtube search is added to the queue.
        """

        song_url = song_url.strip('<>')
        
        if permissions.max_songs and player.playlist.count_for_user(author) >= permissions.max_songs:
            raise exceptions.PermissionsError(
                "You have reached your enqueued song limit (%s)" % permissions.max_songs, expire_in=30
            )

        await self.send_typing(channel)

        if leftover_args:
            song_url = ' '.join([song_url, *leftover_args])

        if re.search('.+watch\?.+&list=',song_url):
            print(re.sub('.+watch\?.+&list=','https://www.youtube.com/playlist?list=',song_url))
            song_url = re.sub('.+watch\?.+&list=','https://www.youtube.com/playlist?list=',song_url)
            
        try:
            info = await self.downloader.extract_info(player.playlist.loop, song_url, download=False, process=False)
        except Exception as e:
            raise exceptions.CommandError(e, expire_in=30)

        if not info:
            raise exceptions.CommandError("That video cannot be played.", expire_in=30)

        if info.get('url', '').startswith('ytsearch'):

            info = await self.downloader.extract_info(
                player.playlist.loop,
                song_url,
                download=False,
                process=True,    
                on_error=lambda e: asyncio.ensure_future(
                    self.safe_send_message(channel, "```\n%s\n```" % e, expire_in=120), loop=self.loop),
                retry_on_error=True
            )

            if not info:
                raise exceptions.CommandError(
                    "Error extracting info from search string, youtubedl returned no data.  "
                    "You may need to restart the bot if this continues to happen.", expire_in=30
                )

            if not all(info.get('entries', [])):

                return

            song_url = info['entries'][0]['webpage_url']
            info = await self.downloader.extract_info(player.playlist.loop, song_url, download=False, process=False)


        if 'entries' in info:

            if not permissions.allow_playlists and ':search' in info['extractor'] and len(info['entries']) > 1:
                raise exceptions.PermissionsError("You are not allowed to request playlists", expire_in=30)

            num_songs = sum(1 for _ in info['entries'])

            if permissions.max_playlist_length and num_songs > permissions.max_playlist_length:
                raise exceptions.PermissionsError(
                    "Playlist has too many entries (%s > %s)" % (num_songs, permissions.max_playlist_length),
                    expire_in=30
                )

            if permissions.max_songs and player.playlist.count_for_user(author) + num_songs > permissions.max_songs:
                raise exceptions.PermissionsError(
                    "Playlist entries + your already queued songs reached limit (%s + %s > %s)" % (
                        num_songs, player.playlist.count_for_user(author), permissions.max_songs),
                    expire_in=30
                )

            if info['extractor'].lower() in ['youtube:playlist', 'soundcloud:set', 'bandcamp:album']:
                try:
                    return await self._cmd_play_playlist_async(player, channel, author, permissions, song_url, info['extractor'])
                except exceptions.CommandError:
                    raise
                except Exception as e:
                    traceback.print_exc()
                    raise exceptions.CommandError("Error queuing playlist:\n%s" % e, expire_in=30)

            t0 = time.time()

            wait_per_song = 1.2

            procmesg = await self.safe_send_message(
                channel,
                'Gathering playlist information for {} songs{}'.format(
                    num_songs,
                    ', ETA: {} seconds'.format(self._fixg(
                        num_songs * wait_per_song)) if num_songs >= 10 else '.'))

            await self.send_typing(channel)

            entry_list, position = await player.playlist.import_from(song_url, channel=channel, author=author)

            tnow = time.time()
            ttime = tnow - t0
            listlen = len(entry_list)
            drop_count = 0

            if permissions.max_song_length:
                for e in entry_list.copy():
                    if e.duration > permissions.max_song_length:
                        player.playlist.entries.remove(e)
                        entry_list.remove(e)
                        drop_count += 1

                if drop_count:
                    print("Dropped %s songs" % drop_count)

            print("Processed {} songs in {} seconds at {:.2f}s/song, {:+.2g}/song from expected ({}s)".format(
                listlen,
                self._fixg(ttime),
                ttime / listlen,
                ttime / listlen - wait_per_song,
                self._fixg(wait_per_song * num_songs))
            )

            await self.safe_delete_message(procmesg)

            if not listlen - drop_count:
                raise exceptions.CommandError(
                    "No songs were added, all songs were over max duration (%ss)" % permissions.max_song_length,
                    expire_in=30
                )

            reply_text = "Enqueued **%s** songs to be played. Position in queue: %s"
            btext = str(listlen - drop_count)

        else:
            if permissions.max_song_length and info.get('duration', 0) > permissions.max_song_length:
                raise exceptions.PermissionsError(
                    "Song duration exceeds limit (%s > %s)" % (info['duration'], permissions.max_song_length),
                    expire_in=30
                )

            try:
                entry, position = await player.playlist.add_entry(song_url, channel=channel, author=author)

            except exceptions.WrongEntryTypeError as e:
                if e.use_url == song_url:
                    print("[Warning] Determined incorrect entry type, but suggested url is the same.  Help.")

                if self.config.debug_mode:
                    print("[Info] Assumed url \"%s\" was a single entry, was actually a playlist" % song_url)
                    print("[Info] Using \"%s\" instead" % e.use_url)

                return await self.cmd_play(player, channel, author, permissions, leftover_args, e.use_url)

            reply_text = "Enqueued **%s** to be played. Position in queue: %s"
            btext = entry.title

        if position == 1 and player.is_stopped:
            position = 'Up next!'
            reply_text %= (btext, position)

        else:
            try:
                time_until = await player.playlist.estimate_time_until(position, player)
                reply_text += ' - estimated time until playing: %s'
            except:
                traceback.print_exc()
                time_until = ''

            reply_text %= (btext, position, time_until)

        if not test.addUser(author.name,1):
            test.addScore(author.name)
        
        append_write = ''
        logstr = '{} {} requested : {}\n'.format(str(datetime.now()), author.name, entry.title)
        print(logstr)
        if os.path.exists('KrBotLog.txt'):
            append_write = 'a' 
        else:
            append_write = 'w' 

        log = open('KrBotLog.txt',append_write,encoding="utf8")
        log.write(logstr)
        log.close()
        
        return Response(reply_text, delete_after=30)        
    
    async def _cmd_play_playlist_async(self, player, channel, author, permissions, playlist_url, extractor_type):
        """
        Secret handler to use the async wizardry to make playlist queuing non-"blocking"
        """

        await self.send_typing(channel)
        info = await self.downloader.extract_info(player.playlist.loop, playlist_url, download=False, process=False)

        if not info:
            raise exceptions.CommandError("That playlist cannot be played.")

        num_songs = sum(1 for _ in info['entries'])
        t0 = time.time()

        busymsg = await self.safe_send_message(
            channel, "Processing %s songs..." % num_songs)  
        await self.send_typing(channel)

        entries_added = 0
        if extractor_type == 'youtube:playlist':
            try:
                entries_added = await player.playlist.async_process_youtube_playlist(
                    playlist_url, channel=channel, author=author)


            except Exception:
                traceback.print_exc()
                raise exceptions.CommandError('Error handling playlist %s queuing.' % playlist_url, expire_in=30)

        elif extractor_type.lower() in ['soundcloud:set', 'bandcamp:album']:
            try:
                entries_added = await player.playlist.async_process_sc_bc_playlist(
                    playlist_url, channel=channel, author=author)


            except Exception:
                traceback.print_exc()
                raise exceptions.CommandError('Error handling playlist %s queuing.' % playlist_url, expire_in=30)


        songs_processed = len(entries_added)
        drop_count = 0
        skipped = False

        if permissions.max_song_length:
            for e in entries_added.copy():
                if e.duration > permissions.max_song_length:
                    try:
                        player.playlist.entries.remove(e)
                        entries_added.remove(e)
                        drop_count += 1
                    except:
                        pass

            if drop_count:
                print("Dropped %s songs" % drop_count)

            if player.current_entry and player.current_entry.duration > permissions.max_song_length:
                await self.safe_delete_message(self.server_specific_data[channel.server]['last_np_msg'])
                self.server_specific_data[channel.server]['last_np_msg'] = None
                skipped = True
                player.skip()
                entries_added.pop()

        await self.safe_delete_message(busymsg)

        songs_added = len(entries_added)
        tnow = time.time()
        ttime = tnow - t0
        wait_per_song = 1.2

        print("Processed {}/{} songs in {} seconds at {:.2f}s/song, {:+.2g}/song from expected ({}s)".format(
            songs_processed,
            num_songs,
            self._fixg(ttime),
            ttime / num_songs,
            ttime / num_songs - wait_per_song,
            self._fixg(wait_per_song * num_songs))
        )

        if not songs_added:
            basetext = "No songs were added, all songs were over max duration (%ss)" % permissions.max_song_length
            if skipped:
                basetext += "\nAdditionally, the current song was skipped for being too long."

            raise exceptions.CommandError(basetext, expire_in=30)

        return Response("Enqueued {} songs to be played in {} seconds".format(
            songs_added, self._fixg(ttime, 1)), delete_after=30)

    async def cmd_search(self, player, channel, author, permissions, leftover_args):
        """
        Usage:
            {command_prefix}search [service] [number] query

        Searches a service for a video and adds it to the queue.
        - service: any one of the following services:
            - youtube (yt) (default if unspecified)
            - soundcloud (sc)
            - yahoo (yh)
        - number: return a number of video results and waits for user to choose one
          - defaults to 1 if unspecified
          - note: If your search query starts with a number,
                  you must put your query in quotes
            - ex: {command_prefix}search 2 "I ran seagulls"
        """

        if permissions.max_songs and player.playlist.count_for_user(author) > permissions.max_songs:
            raise exceptions.PermissionsError(
                "You have reached your playlist item limit (%s)" % permissions.max_songs,
                expire_in=30
            )

        def argcheck():
            if not leftover_args:
                raise exceptions.CommandError(
                    "Please specify a search query.\n%s" % dedent(
                        self.cmd_search.__doc__.format(command_prefix=self.config.command_prefix)),
                    expire_in=60
                )

        argcheck()

        try:
            leftover_args = shlex.split(' '.join(leftover_args))
        except ValueError:
            raise exceptions.CommandError("Please quote your search query properly.", expire_in=30)

        service = 'youtube'
        items_requested = 3
        max_items = 10  
        services = {
            'youtube': 'ytsearch',
            'soundcloud': 'scsearch',
            'yahoo': 'yvsearch',
            'yt': 'ytsearch',
            'sc': 'scsearch',
            'yh': 'yvsearch'
        }

        if leftover_args[0] in services:
            service = leftover_args.pop(0)
            argcheck()

        if leftover_args[0].isdigit():
            items_requested = int(leftover_args.pop(0))
            argcheck()

            if items_requested > max_items:
                raise exceptions.CommandError("You cannot search for more than %s videos" % max_items)

        if leftover_args[0][0] in '\'"':
            lchar = leftover_args[0][0]
            leftover_args[0] = leftover_args[0].lstrip(lchar)
            leftover_args[-1] = leftover_args[-1].rstrip(lchar)

        search_query = '%s%s:%s' % (services[service], items_requested, ' '.join(leftover_args))

        search_msg = await self.send_message(channel, "Searching for videos...")
        await self.send_typing(channel)

        try:
            info = await self.downloader.extract_info(player.playlist.loop, search_query, download=False, process=True)

        except Exception as e:
            await self.safe_edit_message(search_msg, str(e), send_if_fail=True)
            return
        else:
            await self.safe_delete_message(search_msg)

        if not info:
            return Response("No videos found.", delete_after=30)

        def check(m):
            return (
                m.content.lower()[0] in 'yn' or

                m.content.lower().startswith('{}{}'.format(self.config.command_prefix, 'search')) or
                m.content.lower().startswith('exit'))

        for e in info['entries']:
            result_message = await self.safe_send_message(channel, "Result %s/%s: %s" % (
                info['entries'].index(e) + 1, len(info['entries']), e['webpage_url']))

            confirm_message = await self.safe_send_message(channel, "Is this ok? Type `y`, `n` or `exit`")
            response_message = await self.wait_for_message(30, author=author, channel=channel, check=check)

            if not response_message:
                await self.safe_delete_message(result_message)
                await self.safe_delete_message(confirm_message)
                return Response("Ok nevermind.", delete_after=30)

            elif response_message.content.startswith(self.config.command_prefix) or \
                    response_message.content.lower().startswith('exit'):

                await self.safe_delete_message(result_message)
                await self.safe_delete_message(confirm_message)
                return

            if response_message.content.lower().startswith('y'):
                await self.safe_delete_message(result_message)
                await self.safe_delete_message(confirm_message)
                await self.safe_delete_message(response_message)

                await self.cmd_play(player, channel, author, permissions, [], e['webpage_url'])

                return Response("Alright, coming right up!", delete_after=30)
            else:
                await self.safe_delete_message(result_message)
                await self.safe_delete_message(confirm_message)
                await self.safe_delete_message(response_message)

        return Response("Oh well :frowning:", delete_after=30)
    
    async def cmd_hardreset(self, channel):
        """
        Usage:
            {command_prefix}hardreset

        Performs a hard reset on the bot.
        """    
        subprocess.Popen(['C:\\Users\\Juven\\MusicBot\\runbot.bat'])
        return await(self.cmd_shutdown(channel))
        
    async def cmd_np(self, player, channel, server, message):
        """
        Usage:
            {command_prefix}np

        Displays the current song in chat.
        """

        if player.current_entry:
            if self.server_specific_data[server]['last_np_msg']:
                await self.safe_delete_message(self.server_specific_data[server]['last_np_msg'])
                self.server_specific_data[server]['last_np_msg'] = None

            song_progress = str(timedelta(seconds=player.progress)).lstrip('0').lstrip(':')
            song_total = str(timedelta(seconds=player.current_entry.duration)).lstrip('0').lstrip(':')
            prog_str = '`[%s/%s]`' % (song_progress, song_total)
            
            prog_bar_str = ''

            percentage = 0.0
            if player.current_entry.duration > 0:
                percentage = player.progress / player.current_entry.duration
            progress_bar_length = 30
            for i in range(progress_bar_length):
                if (percentage < 1 / progress_bar_length * i):
                    prog_bar_str += '□'
                else:
                    prog_bar_str += '■'

            if player.current_entry.meta.get('channel', False) and player.current_entry.meta.get('author', False):
                np_text = "Now Playing: **{title}** added by **{author}**\nProgress: {progress_bar} {progress}\n\N{WHITE RIGHT POINTING BACKHAND INDEX} <{url}>".format(
                    title=player.current_entry.title,
                    author=player.current_entry.meta['author'].name,
                    progress_bar=prog_bar_str,
                    progress=prog_str,
                    url=player.current_entry.url
                )
            else:
                np_text = "Now Playing: **{title}**\nProgress: {progress_bar} {progress}\n\N{WHITE RIGHT POINTING BACKHAND INDEX} <{url}>".format(
                    title=player.current_entry.title,
                    progress_bar=prog_bar_str,
                    progress=prog_str,
                    url=player.current_entry.url
                )

            self.server_specific_data[server]['last_np_msg'] = await self.safe_send_message(channel, np_text)
            await self._manual_delete_check(message)
        else:
            return Response(
                'There are no songs queued! Queue something with {}play.'.format(self.config.command_prefix),
                delete_after=30
            )

    async def cmd_summon(self, channel, author, voice_channel):
        """
        Usage:
            {command_prefix}summon

        Call the bot to the summoner's voice channel.
        """

        if not author.voice_channel:
            raise exceptions.CommandError('You are not in a voice channel!')

        voice_client = self.the_voice_clients.get(channel.server.id, None)
        if voice_client and voice_client.channel.server == author.voice_channel.server:
            await self.move_voice_client(author.voice_channel)
            return

        chperms = author.voice_channel.permissions_for(author.voice_channel.server.me)

        if not chperms.connect:
            self.safe_print("Cannot join channel \"%s\", no permission." % author.voice_channel.name)
            return Response(
                "```Cannot join channel \"%s\", no permission.```" % author.voice_channel.name,
                delete_after=25
            )

        elif not chperms.speak:
            self.safe_print("Will not join channel \"%s\", no permission to speak." % author.voice_channel.name)
            return Response(
                "```Will not join channel \"%s\", no permission to speak.```" % author.voice_channel.name,
                delete_after=25
            )

        player = await self.get_player(author.voice_channel, create=True)

        if player.is_stopped:
            player.play()

        if self.config.auto_playlist:
            await self.on_player_finished_playing(player)

    async def cmd_pause(self, player):
        """
        Usage:
            {command_prefix}pause

        Pauses playback of the current song.
        """

        if player.is_playing:
            player.pause()

        else:
            raise exceptions.CommandError('Player is not playing.', expire_in=30)

    async def cmd_resume(self, player):
        """
        Usage:
            {command_prefix}resume

        Resumes playback of a paused song.
        """

        if player.is_paused:
            player.resume()

        else:
            raise exceptions.CommandError('Player is not paused.', expire_in=30)

    async def cmd_shuffle(self, channel, player, loop = None):
        """
        Usage:
            {command_prefix}shuffle <optional : shuffle count>

        Shuffles the playlist, if argument is specified, shuffles that many times
        """
        if loop is None:
            r = 1
        else:
            if loop.isdigit():
                r = int(loop)
            else:
                return Response('**Argument specified is not a number**')
        for x in range(r):
            player.playlist.shuffle()

            cards = [':spades:',':clubs:',':hearts:',':diamonds:']
            hand = await self.send_message(channel, ' '.join(cards))
            await asyncio.sleep(0.6)

            for x in range(4):
                shuffle(cards)
                await self.safe_edit_message(hand, ' '.join(cards))
                await asyncio.sleep(0.6)

            await self.safe_delete_message(hand, quiet=True)
        return Response(":ok_hand:", delete_after=15)

    async def cmd_clear(self, player, author):
        """
        Usage:
            {command_prefix}clear

        Clears the playlist.
        """

        player.playlist.clear()
        return Response(':put_litter_in_its_place:', delete_after=20)

    async def cmd_skip(self, player, channel, author, message, permissions, voice_channel):
        """
        Usage:
            {command_prefix}skip

        Skips the current song when enough votes are cast, or by the bot owner.
        """

        if player.is_stopped:
            raise exceptions.CommandError("Can't skip! The player is not playing!", expire_in=20)

        if not player.current_entry:
            if player.playlist.peek():
                if player.playlist.peek()._is_downloading:

                    return Response("The next song (%s) is downloading, please wait." % player.playlist.peek().title)

                elif player.playlist.peek().is_downloaded:
                    print("The next song will be played shortly.  Please wait.")
                else:
                    print("Something odd is happening.  "
                          "You might want to restart the bot if it doesn't start working.")
            else:
                print("Something strange is happening.  "
                      "You might want to restart the bot if it doesn't start working.")

        if author.id == self.config.owner_id \
                or permissions.instaskip \
                or author == player.current_entry.meta.get('author', None):

            player.skip()  
            await self._manual_delete_check(message)
            return


        num_voice = sum(1 for m in voice_channel.voice_members if not (
            m.deaf or m.self_deaf or m.id in [self.config.owner_id, self.user.id]))

        num_skips = player.skip_state.add_skipper(author.id, message)

        skips_remaining = min(self.config.skips_required,
                              sane_round_int(num_voice * self.config.skip_ratio_required)) - num_skips

        if skips_remaining <= 0:
            player.skip()  
            return Response(
                'your skip for **{}** was acknowledged.'
                '\nThe vote to skip has been passed.{}'.format(
                    player.current_entry.title,
                    ' Next song coming up!' if player.playlist.peek() else ''
                ),
                reply=True,
                delete_after=20
            )

        else:

            return Response(
                'your skip for **{}** was acknowledged.'
                '\n**{}** more {} required to vote to skip this song.'.format(
                    player.current_entry.title,
                    skips_remaining,
                    'person is' if skips_remaining == 1 else 'people are'
                ),
                reply=True,
                delete_after=20
            )

    async def cmd_volume(self, message, player, new_volume=None):
        """
        Usage:
            {command_prefix}volume (+/-)[volume]

        Sets the playback volume. Accepted values are from 1 to 100.
        Putting + or - before the volume will make the volume change relative to the current volume.
        """

        if not new_volume:
            return Response('Current volume: `%s%%`' % int(player.volume * 100), reply=True, delete_after=20)

        relative = False
        if new_volume[0] in '+-':
            relative = True

        try:
            new_volume = int(new_volume)

        except ValueError:
            raise exceptions.CommandError('{} is not a valid number'.format(new_volume), expire_in=20)

        if relative:
            vol_change = new_volume
            new_volume += (player.volume * 100)

        old_volume = int(player.volume * 100)

        if 0 < new_volume <= 100:
            player.volume = new_volume / 100.0

            return Response('updated volume from %d to %d' % (old_volume, new_volume), reply=True, delete_after=20)

        else:
            if relative:
                raise exceptions.CommandError(
                    'Unreasonable volume change provided: {}{:+} -> {}%.  Provide a change between {} and {:+}.'.format(
                        old_volume, vol_change, old_volume + vol_change, 1 - old_volume, 100 - old_volume), expire_in=20)
            else:
                raise exceptions.CommandError(
                    'Unreasonable volume provided: {}%. Provide a value between 1 and 100.'.format(new_volume), expire_in=20)

    async def cmd_queue(self, channel, player):
        """
        Usage:
            {command_prefix}queue

        Prints the current song queue.
        """

        lines = []
        unlisted = 0
        andmoretext = '* ... and %s more*' % ('x' * len(player.playlist.entries))

        if player.current_entry:
            song_progress = str(timedelta(seconds=player.progress)).lstrip('0').lstrip(':')
            song_total = str(timedelta(seconds=player.current_entry.duration)).lstrip('0').lstrip(':')
            prog_str = '`[%s/%s]`' % (song_progress, song_total)

            if player.current_entry.meta.get('channel', False) and player.current_entry.meta.get('author', False):
                lines.append("Now Playing: **%s** added by **%s** %s\n" % (
                    player.current_entry.title, player.current_entry.meta['author'].name, prog_str))
            else:
                lines.append("Now Playing: **%s** %s\n" % (player.current_entry.title, prog_str))

        for i, item in enumerate(player.playlist, 1):
            if item.meta.get('channel', False) and item.meta.get('author', False):
                nextline = '`{}.` **{}** added by **{}**'.format(i, item.title, item.meta['author'].name).strip()
            else:
                nextline = '`{}.` **{}**'.format(i, item.title).strip()

            currentlinesum = sum(len(x) + 1 for x in lines)  

            if currentlinesum + len(nextline) + len(andmoretext) > DISCORD_MSG_CHAR_LIMIT:
                if currentlinesum + len(andmoretext):
                    unlisted += 1
                    continue

            lines.append(nextline)

        if unlisted:
            lines.append('\n*... and %s more*' % unlisted)

        if not lines:
            lines.append(
                'There are no songs queued! Queue something with {}play.'.format(self.config.command_prefix))

        message = '\n'.join(lines)
        return Response(message, delete_after=30)

    async def cmd_clean(self, message, channel, server, author, search_range=50):
        """
        Usage:
            {command_prefix}clean [range]

        Removes up to [range] messages the bot has posted in chat. Default: 50, Max: 1000
        """

        try:
            float(search_range)  
            search_range = min(int(search_range), 1000)
        except:
            return Response("enter a number.  NUMBER.  That means digits.  `15`.  Etc.", reply=True, delete_after=8)

        await self.safe_delete_message(message, quiet=True)

        def is_possible_command_invoke(entry):
            valid_call = any(
                entry.content.startswith(prefix) for prefix in [self.config.command_prefix])  
            return valid_call and not entry.content[1:2].isspace()

        delete_invokes = True
        delete_all = channel.permissions_for(author).manage_messages or self.config.owner_id == author.id

        def check(message):
            if is_possible_command_invoke(message) and delete_invokes:
                return delete_all or message.author == author
            return message.author == self.user

        if self.user.bot:
            if channel.permissions_for(server.me).manage_messages:
                deleted = await self.purge_from(channel, check=check, limit=search_range, before=message)
                return Response('Cleaned up {} message{}.'.format(len(deleted), 's' * bool(deleted)), delete_after=15)

        deleted = 0
        async for entry in self.logs_from(channel, search_range, before=message):
            if entry == self.server_specific_data[channel.server]['last_np_msg']:
                continue

            if entry.author == self.user:
                await self.safe_delete_message(entry)
                deleted += 1
                await asyncio.sleep(0.21)

            if is_possible_command_invoke(entry) and delete_invokes:
                if delete_all or entry.author == author:
                    try:
                        await self.delete_message(entry)
                        await asyncio.sleep(0.21)
                        deleted += 1

                    except discord.Forbidden:
                        delete_invokes = False
                    except discord.HTTPException:
                        pass

        return Response('Cleaned up {} message{}.'.format(deleted, 's' * bool(deleted)), delete_after=15)

    async def cmd_pldump(self, channel, song_url):
        """
        Usage:
            {command_prefix}pldump url

        Dumps the individual urls of a playlist
        """

        try:
            info = await self.downloader.extract_info(self.loop, song_url.strip('<>'), download=False, process=False)
        except Exception as e:
            raise exceptions.CommandError("Could not extract info from input url\n%s\n" % e, expire_in=25)

        if not info:
            raise exceptions.CommandError("Could not extract info from input url, no data.", expire_in=25)

        if not info.get('entries', None):


            if info.get('url', None) != info.get('webpage_url', info.get('url', None)):
                raise exceptions.CommandError("This does not seem to be a playlist.", expire_in=25)
            else:
                return await self.cmd_pldump(channel, info.get(''))

        linegens = defaultdict(lambda: None, **{
            "youtube":    lambda d: 'https://www.youtube.com/watch?v=%s' % d['id'],
            "soundcloud": lambda d: d['url'],
            "bandcamp":   lambda d: d['url']
        })

        exfunc = linegens[info['extractor'].split(':')[0]]

        if not exfunc:
            raise exceptions.CommandError("Could not extract info from input url, unsupported playlist type.", expire_in=25)

        with BytesIO() as fcontent:
            for item in info['entries']:
                fcontent.write(exfunc(item).encode('utf8') + b'\n')

            fcontent.seek(0)
            await self.send_file(channel, fcontent, filename='playlist.txt', content="Here's the url dump for <%s>" % song_url)

        return Response(":mailbox_with_mail:", delete_after=20)

    async def cmd_insertlibrary(self, author, player, song_url, type):
        """
        Usage:
            {command_prefix}insertlibrary <song url> <library to add to>

        Adds a song to known libraries.
        """
        
        try:
            info = await self.downloader.extract_info(player.playlist.loop, song_url, download=False, process=False)
        except Exception as e:
            raise exceptions.CommandError(e, expire_in=30)

        if not info:
            raise exceptions.CommandError("That video cannot be played.", expire_in=30)        

        linegens = defaultdict(lambda: None, **{
            "youtube":    lambda d: 'https://www.youtube.com/watch?v=%s' % d['id'],
            "soundcloud": lambda d: d['url'],
            "bandcamp":   lambda d: d['url']
        })

        exfunc = linegens[info['extractor'].split(':')[0]]
        
        if type.lower() in self.get_libraries():  
            if open('libs/' + type.lower() + '.txt','r',encoding="utf8").readlines().count(song_url+'\n') != 0:
                return Response('**That song is already in the library!**', reply = True, delete_after=40)
            file_write = ''
            append_write = ''
            if os.path.exists('libs/' + type.lower() + '.txt'):
                 file_write = 'a' 
            else:
                 file_write = 'w' 
            if os.path.exists('KrBotLog.txt'):
                 append_write = 'a' 
            else:
                 append_write = 'w' 

            f = open('libs/' + type.lower() + '.txt',file_write,encoding="utf8")
            log = open('KrBotLog.txt',append_write,encoding="utf8")
            f.write(song_url+'\n')
            logstr = '{} {} Added to library : {}\n'.format(str(datetime.now()), author.name, info['title'])               
            log.write(logstr)
            log.close()
            f.close()        
        else:
            return Response('**That library does not exists! type !libraries to get list of known libraries**', reply = True, delete_after = 60)
            
        return Response('**{} song library updated! Added -> {}**'.format(type,info['title']), reply=True, delete_after=40)

    async def cmd_liblength(self, type):
        """
        Usage:
            {command_prefix}liblength <name of library>

        Gets the length of a song library.
        """   
        if type.lower() in self.get_libraries():
            return Response('**The library {} has {} songs.**'.format(type, len(open('libs/' + type + '.txt','r',encoding="utf8").readlines())), delete_after = 60)
        else:
            return Response('**That library does not exists! type !libraries to get list of known libraries**', reply = True, delete_after = 60)        
        
    async def cmd_appendlibrary(self, author, song_url, type):
        """
        Usage:
            {command_prefix}appendlibrary <playlist url> <library to add to>

        Adds a playlist to known libraries.
        """

        try:
            info = await self.downloader.extract_info(self.loop, song_url.strip('<>'), download=False, process=False)
        except Exception as e:
            raise exceptions.CommandError("Could not extract info from input url\n%s\n" % e, expire_in=25)

        if not info:
            raise exceptions.CommandError("Could not extract info from input url, no data.", expire_in=25)

        if not info.get('entries', None):


            if info.get('url', None) != info.get('webpage_url', info.get('url', None)):
                raise exceptions.CommandError("This does not seem to be a playlist.", expire_in=25)
            else:
                return await self.cmd_pldump(channel, info.get(''))

        linegens = defaultdict(lambda: None, **{
            "youtube":    lambda d: 'https://www.youtube.com/watch?v=%s' % d['id'],
            "soundcloud": lambda d: d['url'],
            "bandcamp":   lambda d: d['url']
        })

        exfunc = linegens[info['extractor'].split(':')[0]]

        if not exfunc:
            raise exceptions.CommandError("Could not extract info from input url, unsupported playlist type.", expire_in=25)
        
        count = 0
        if type.lower() in self.get_libraries():    
            list_write = ''
            append_write = ''
            if os.path.exists('libs/' + type.lower() + '.txt'):
                 list_write = 'a' 
            else:
                 list_write = 'w' 
            if os.path.exists('KrBotLog.txt'):
                 append_write = 'a' 
            else:
                 append_write = 'w' 

            listfile = open('libs/' + type + '.txt',list_write,encoding="utf8")
            log = open('KrBotLog.txt',append_write,encoding="utf8")
            for item in info['entries']:
                listfile.write(exfunc(item)+'\n')
                logstr = '{} {} Added to library : {}\n'.format(str(datetime.now()), author.name, item['title'])               
                log.write(logstr)
                count+=1
            log.close()
            listfile.close()    
        else:
            return Response('**That library does not exists! type !libraries to get list of known libraries**', reply = True, delete_after = 60)

        return Response('**{} song library updated! Songs added -> {}**'.format(type,count), reply=True, delete_after=40)

    async def cmd_reset(self, player):
        """
        Usage:
            {command_prefix}reset
        Resets the settings for the player
        """  
        player.speed = 1
        player.sample_rate = 48000
        player.bass = 0
        return Response('**Default settings applied**')
        
    async def cmd_normalized_pitch(self, player, rate=None):
        """
        Usage:
            {command_prefix}normalized_pitch <optional : value>
        Test command, modifies pitch and speed accordingly
        If rate is specified, sets the pitch of the player (min 0.5 , max 2), otherwise prints the current pitch
        """  
        if rate is None:
            return Response('**Current pitch is {:.02f}, speed is {:.02f}**'.format(player.sample_rate/48000,player.speed),delete_after=60)
        else:
            try:
                val = float(rate)
                if val < 0.5 or val > 2:
                    return Response('**Not supported yet, min is 0.5 and max is 2.0**')
                player.speed = 1
                player.sample_rate = val * 48000
                player.speed = 1 / val
                return Response('**Pitch set to {}, speed set to {:.02f}**'.format(val,player.speed))
            except ValueError:
                return Response('**Specified argument is not a valid number**')
    
    async def cmd_pitch(self, player, rate=None):
        """
        Usage:
            {command_prefix}pitch <optional : value>

        If rate is specified, sets the pitch of the player (min 0), otherwise prints the current pitch
        """  
        if rate is None:
            return Response('**Current pitch is {:.02f}**'.format(player.sample_rate/48000),delete_after=60)
        else:
            try:
                val = float(rate)
                player.sample_rate = val * 48000
                return Response('**Pitch set to {}**'.format(val))
            except ValueError:
                return Response('**Specified argument is not a valid number**')
                
    async def cmd_speed(self, player, speed=None):
        """
        Usage:
            {command_prefix}speed <optional : value>

        If speed is specified, sets the play speed of the player (between 0.5 and 4.0), otherwise prints the current speed
        """  
        if speed is None:
            return Response('**Current play speed is {}**'.format(player.speed),delete_after=60)
        else:
            try:
                val = float(speed)
                if val < 0.5 or val > 4.0:
                    return Response('**Use values between 0.5 and 4.0**', delete_after=35)
                player.speed = val
                return Response('**Play speed set to {}**'.format(val))
            except ValueError:
                return Response('**Specified argument is not a valid number**')
    
    async def cmd_listids(self, server, author, leftover_args, cat='all'):
        """
        Usage:
            {command_prefix}listids [categories]

        Lists the ids for various things.  Categories are:
           all, users, roles, channels
        """

        cats = ['channels', 'roles', 'users']

        if cat not in cats and cat != 'all':
            return Response(
                "Valid categories: " + ' '.join(['`%s`' % c for c in cats]),
                reply=True,
                delete_after=25
            )

        if cat == 'all':
            requested_cats = cats
        else:
            requested_cats = [cat] + [c.strip(',') for c in leftover_args]

        data = ['Your ID: %s' % author.id]

        for cur_cat in requested_cats:
            rawudata = None

            if cur_cat == 'users':
                data.append("\nUser IDs:")
                rawudata = ['%s #%s: %s' % (m.name, m.discriminator, m.id) for m in server.members]

            elif cur_cat == 'roles':
                data.append("\nRole IDs:")
                rawudata = ['%s: %s' % (r.name, r.id) for r in server.roles]

            elif cur_cat == 'channels':
                data.append("\nText Channel IDs:")
                tchans = [c for c in server.channels if c.type == discord.ChannelType.text]
                rawudata = ['%s: %s' % (c.name, c.id) for c in tchans]

                rawudata.append("\nVoice Channel IDs:")
                vchans = [c for c in server.channels if c.type == discord.ChannelType.voice]
                rawudata.extend('%s: %s' % (c.name, c.id) for c in vchans)

            if rawudata:
                data.extend(rawudata)

        with BytesIO() as sdata:
            sdata.writelines(d.encode('utf8') + b'\n' for d in data)
            sdata.seek(0)

            await self.send_file(author, sdata, filename='%s-ids-%s.txt' % (server.name.replace(' ', '_'), cat))

        return Response(":mailbox_with_mail:", delete_after=20)


    async def cmd_perms(self, author, channel, server, permissions):
        """
        Usage:
            {command_prefix}perms

        Sends the user a list of their permissions.
        """

        lines = ['Command permissions in %s\n' % server.name, '```', '```']

        for perm in permissions.__dict__:
            if perm in ['user_list'] or permissions.__dict__[perm] == set():
                continue

            lines.insert(len(lines) - 1, "%s: %s" % (perm, permissions.__dict__[perm]))

        await self.send_message(author, '\n'.join(lines))
        return Response(":mailbox_with_mail:", delete_after=20)


    @owner_only
    async def cmd_setname(self, leftover_args, name):
        """
        Usage:
            {command_prefix}setname name

        Changes the bot's username.
        Note: This operation is limited by discord to twice per hour.
        """

        name = ' '.join([name, *leftover_args])

        try:
            await self.edit_profile(username=name)
        except Exception as e:
            raise exceptions.CommandError(e, expire_in=20)

        return Response(":ok_hand:", delete_after=20)

    @owner_only
    async def cmd_setnick(self, server, channel, leftover_args, nick):
        """
        Usage:
            {command_prefix}setnick nick

        Changes the bot's nickname.
        """

        if not channel.permissions_for(server.me).change_nickname:
            raise exceptions.CommandError("Unable to change nickname: no permission.")

        nick = ' '.join([nick, *leftover_args])

        try:
            await self.change_nickname(server.me, nick)
        except Exception as e:
            raise exceptions.CommandError(e, expire_in=20)

        return Response(":ok_hand:", delete_after=20)

    @owner_only
    async def cmd_setavatar(self, message, url=None):
        """
        Usage:
            {command_prefix}setavatar [url]

        Changes the bot's avatar.
        Attaching a file and leaving the url parameter blank also works.
        """

        if message.attachments:
            thing = message.attachments[0]['url']
        else:
            thing = url.strip('<>')

        try:
            with aiohttp.Timeout(10):
                async with self.aiosession.get(thing) as res:
                    await self.edit_profile(avatar=await res.read())

        except Exception as e:
            raise exceptions.CommandError("Unable to change avatar: %s" % e, expire_in=20)

        return Response(":ok_hand:", delete_after=20)


    async def cmd_disconnect(self, server):
        await self.disconnect_voice_client(server)
        return Response(":hear_no_evil:", delete_after=20)

    async def cmd_restart(self, channel):
        await self.safe_send_message(channel, ":wave:")
        await self.disconnect_all_voice_clients()
        raise exceptions.RestartSignal

    @owner_only
    async def cmd_shutdown(self, channel):
        await self.safe_send_message(channel, ":wave:")
        await self.disconnect_all_voice_clients()
        raise exceptions.TerminateSignal

    async def on_message_edit(self, before, after):
        append_write = ''
        logstr = '{} {} edited : {} **TO** {}\n'.format(str(datetime.now()), before.author.name, before.content.strip(), after.content.strip())
        if os.path.exists('ChatLog.txt'):
            append_write = 'a' 
        else:
            append_write = 'w' 
        log = open('ChatLog.txt',append_write,encoding="utf8")
        log.write(logstr)
        log.close()
        print(logstr)

    async def cmd_ntf(self, channel):
        """
        Usage:
            {command_prefix}ntf
        NANI THE FUCK
        """
        pics = os.listdir('./ntf')
        maxnum = len(pics)
        selected = random.randint(0,maxnum)
        return await self.send_file(channel, './ntf/{}'.format(pics[selected]))    
        
    async def cmd_bass_boost(self, player, rate=None):
        """
        Usage:
            {command_prefix}bass_boost <optional : value>
        Test command, sets gain value for low frequencies
        If rate is specified, sets the gain for low frequencies (min -10, max 10), otherwise prints the current gain value
        """
        if rate is None:
            return Response('**Current bass gain is {}**'.format(player.bass),delete_after=60)
        else:
            try:
                val = int(rate)
                if val < -10 or val > 10:
                    return Response('**Unsupported operation, check min max value**')
                player.bass = val
                return Response('**Bass gain set to {}**'.format(player.bass))
            except ValueError:
                return Response('**Specified argument is not a valid number**')        
        
    async def on_message(self, message):
        await self.wait_until_ready()

        message_content = message.content.strip()

        if message.author.id in self.muted:
            return await self.safe_delete_message(message)
        
        if message_content.startswith(self.config.command_prefix):
            if message.author.id in self.banned:
                self.safe_delete_message(message)
                return
        
        if not message_content.startswith(self.config.command_prefix):
            if not message.author == self.user and 'bots' not in [obj.name.lower() for obj in message.author.roles] and len(message_content.lower().replace(self.user.name.lower(),'')) > 0:
                if self.user.name.lower() in message_content.lower():
                    if self.chat_enabled == False and not message.channel == '194719115477254144':
                        return
                    c = 0
                    for x in message_content.lower().split(' '):
                        if x in ['set','volume','to']:
                            c += 1
                    if c > 1:
                        return await self.safe_send_message(message.channel,'Just do it yourself')
                    s = message_content.lower().replace(self.user.name.lower(),'').replace(',','').strip()
                else:
                    if message.channel.id == '359400561612816385':
                        if message_content.lower() == 'chat on':
                            self.chat_enabled = True
                            return
                        if message_content.lower() == 'chat off':
                            self.chat_enabled = False
                            return
                        return await self.send_message(discord.Client.get_channel(self,id='194719115477254144'), message_content, tts=self.chat_enabled)
                append_write = ''
                logstr = '{} {} said : {}\n'.format(str(datetime.now()), message.author.name, message_content)
                if os.path.exists('ChatLog.txt'):
                    append_write = 'a' 
                else:
                    append_write = 'w' 

                log = open('ChatLog.txt',append_write,encoding="utf8")
                log.write(logstr)
                log.close()
            return


            
        if message.author == self.user:
            self.safe_print("Ignoring command from myself (%s)" % message.content)
            return

        if self.config.bound_channels and message.channel.id not in self.config.bound_channels and not message.channel.is_private:
            return  

        command, *args = message_content.split()  
        command = command[len(self.config.command_prefix):].lower().strip()

        handler = getattr(self, 'cmd_%s' % command, None)
        if not handler:
            return

        append_write = ''
        logstr = '{} {} Command : {}\n'.format(str(datetime.now()), message.author.name, message_content)
        if os.path.exists('CommandsLog.txt'):
            append_write = 'a' 
        else:
            append_write = 'w' 

        log = open('CommandsLog.txt',append_write,encoding="utf8")
        log.write(logstr)
        log.close()    
            
        if message.channel.is_private:
            if not (message.author.id == self.config.owner_id and command == 'joinserver'):
                await self.send_message(message.channel, 'You cannot use this bot in private messages.')
                return

        if message.author.id in self.blacklist and message.author.id != self.config.owner_id:
            self.safe_print("[User blacklisted] {0.id}/{0.name} ({1})".format(message.author, message_content))
            return

        else:
            self.safe_print("[Command] {0.id}/{0.name} ({1})".format(message.author, message_content))

        user_permissions = self.permissions.for_user(message.author)

        argspec = inspect.signature(handler)
        params = argspec.parameters.copy()

        try:
            if user_permissions.ignore_non_voice and command in user_permissions.ignore_non_voice:
                await self._check_ignore_non_voice(message)

            handler_kwargs = {}
            if params.pop('message', None):
                handler_kwargs['message'] = message

            if params.pop('channel', None):
                handler_kwargs['channel'] = message.channel

            if params.pop('author', None):
                handler_kwargs['author'] = message.author

            if params.pop('server', None):
                handler_kwargs['server'] = message.server

            if params.pop('player', None):
                handler_kwargs['player'] = await self.get_player(message.channel)

            if params.pop('permissions', None):
                handler_kwargs['permissions'] = user_permissions

            if params.pop('user_mentions', None):
                handler_kwargs['user_mentions'] = list(map(message.server.get_member, message.raw_mentions))

            if params.pop('channel_mentions', None):
                handler_kwargs['channel_mentions'] = list(map(message.server.get_channel, message.raw_channel_mentions))

            if params.pop('voice_channel', None):
                handler_kwargs['voice_channel'] = message.server.me.voice_channel

            if params.pop('leftover_args', None):
                handler_kwargs['leftover_args'] = args

            args_expected = []
            for key, param in list(params.items()):
                doc_key = '[%s=%s]' % (key, param.default) if param.default is not inspect.Parameter.empty else key
                args_expected.append(doc_key)

                if not args and param.default is not inspect.Parameter.empty:
                    params.pop(key)
                    continue

                if args:
                    arg_value = args.pop(0)
                    handler_kwargs[key] = arg_value
                    params.pop(key)

            if message.author.id != self.config.owner_id:
                if user_permissions.command_whitelist and command not in user_permissions.command_whitelist:
                    raise exceptions.PermissionsError(
                        "This command is not enabled for your group (%s)." % user_permissions.name,
                        expire_in=20)

                elif user_permissions.command_blacklist and command in user_permissions.command_blacklist:
                    raise exceptions.PermissionsError(
                        "This command is disabled for your group (%s)." % user_permissions.name,
                        expire_in=20)

            if params:
                docs = getattr(handler, '__doc__', None)
                if not docs:
                    docs = 'Usage: {}{} {}'.format(
                        self.config.command_prefix,
                        command,
                        ' '.join(args_expected)
                    )

                docs = '\n'.join(l.strip() for l in docs.split('\n'))
                await self.safe_send_message(
                    message.channel,
                    '```\n%s\n```' % docs.format(command_prefix=self.config.command_prefix),
                    expire_in=60
                )
                return

            response = await handler(**handler_kwargs)
            if response and isinstance(response, Response):
                content = response.content
                if response.reply:
                    content = '%s, %s' % (message.author.mention, content)

                sentmsg = await self.safe_send_message(
                    message.channel, content,
                    expire_in=response.delete_after if self.config.delete_messages else 0,
                    also_delete=message if self.config.delete_invoking else None
                )

        except (exceptions.CommandError, exceptions.HelpfulError, exceptions.ExtractionError) as e:
            print("{0.__class__}: {0.message}".format(e))

            expirein = e.expire_in if self.config.delete_messages else None
            alsodelete = message if self.config.delete_invoking else None

            await self.safe_send_message(
                message.channel,
                '```\n%s\n```' % e.message,
                expire_in=expirein,
                also_delete=alsodelete
            )

        except exceptions.Signal:
            raise

        except Exception:
            traceback.print_exc()
            if self.config.debug_mode:
                await self.safe_send_message(message.channel, '```\n%s\n```' % traceback.format_exc())

    async def on_voice_state_update(self, before, after):
        if not all([before, after]):
            return

        if before.voice_channel == after.voice_channel:
            return

        if before.server.id not in self.players:
            return

        my_voice_channel = after.server.me.voice_channel  

        if not my_voice_channel:
            return

        if before.voice_channel == my_voice_channel:
            joining = False
        elif after.voice_channel == my_voice_channel:
            joining = True
        else:
            return  

        moving = before == before.server.me

        auto_paused = self.server_specific_data[after.server]['auto_paused']
        player = await self.get_player(my_voice_channel)

        if after == after.server.me and after.voice_channel:
            player.voice_client.channel = after.voice_channel

        if not self.config.auto_pause:
            return

        if sum(1 for m in my_voice_channel.voice_members if m != after.server.me):
            if auto_paused and player.is_paused:
                print("[config:autopause] Unpausing")
                self.server_specific_data[after.server]['auto_paused'] = False
                player.resume()
        else:
            if not auto_paused and player.is_playing:
                print("[config:autopause] Pausing")
                self.server_specific_data[after.server]['auto_paused'] = True
                player.pause()

    async def on_server_update(self, before:discord.Server, after:discord.Server):
        if before.region != after.region:
            self.safe_print("[Servers] \"%s\" changed regions: %s -> %s" % (after.name, before.region, after.region))

            await self.reconnect_voice_client(after)

    
    async def cmd_rekt(self):
        return Response("http://giphy.com/gifs/rekt-vSR0fhtT5A9by", reply=False, delete_after=20)
    
    async def cmd_killme(self):
        return Response("http://giphy.com/gifs/kill-luuTVxaoU1JMQ", reply=False, delete_after=20)    
  
    async def cmd_flame(self, user_mentions, author):
        """
        Usage:
            {command_prefix}flame [@user]

            Tells that user to fuck themselves.
        """
        usr = user_mentions[0]
        if usr.name.lower() == 'krbot':
            return Response("Nice try bitch, %s go kill yourself nigger!" % (author.name), reply=True, delete_after=35)
        else:
            return Response("%s, go fuck yourself you nigger" % (usr.name), reply=False, delete_after=35)    
    
    async def cmd_promote(self, player, channel, author, message, permissions, voice_channel, val=None):
        """
        Usage:
            {command_prefix}promote [index]
            
            Promotes the song in the queue to be played after the current song.
        """
        
        if val is None:
            return Response('**No index specified, use help command**')
        
        try:
            idx = int(val)
            idx = idx - 1
            if idx >= 0 and idx < len(player.playlist.entries):
                temp_entry = player.playlist.entries[idx]
                player.playlist.remove_entry(idx)
                player.playlist.entries.appendleft(temp_entry)
                return Response('**Your song has been promoted**', reply=True, delete_after=40)
            else:
                return Response('**Invalid index specified**', reply=True, delete_after=40)
        except ValueError:
            return Response('**Invalid index specified**', reply=True, delete_after=40)
            
    
    async def cmd_jump(self, player, channel, author, message, permissions, voice_channel, val=None):
        """
        Usage:
            {command_prefix}jump [number of skips]

            Skips sepcified number of songs from the playlist. NOTE: Current playing song is not skipped!
        """
        
        count = 0
        total_skips = int(val)
        while(count < total_skips):
            print(count, ' ', total_skips)
            player.playlist.entries.popleft()
            count = count + 1
        return Response("Skipped %s Songs From Playlist!" % (count), reply=False, delete_after=35)
        
    async def cmd_ranks(self):
        return Response(test.getRanks(), reply=False, delete_after=35)

    async def cmd_registerstreamer(self, streamerlink):
        """
        Usage:
            {command_prefix} registerstreamer link
            
        Adds the streamer with the link to the watch list so the bot will monitor if it goes live.
        """
        
        
    
    async def cmd_playnext(self, player, channel, author, permissions, leftover_args, song_url):
        """
        Usage:
            {command_prefix}play song_link
            {command_prefix}play text to search for

        Adds the song to the start of the playlist.  If a link is not provided, the first
        result from a youtube search is added to the queue.
        """

        song_url = song_url.strip('<>')

        if permissions.max_songs and player.playlist.count_for_user(author) >= permissions.max_songs:
            raise exceptions.PermissionsError(
                "You have reached your enqueued song limit (%s)" % permissions.max_songs, expire_in=30
            )

        await self.send_typing(channel)

        if leftover_args:
            song_url = ' '.join([song_url, *leftover_args])

        try:
            info = await self.downloader.extract_info(player.playlist.loop, song_url, download=False, process=False)
        except Exception as e:
            raise exceptions.CommandError(e, expire_in=30)

        if not info:
            raise exceptions.CommandError("That video cannot be played.", expire_in=30)

        if info.get('url', '').startswith('ytsearch'):

            info = await self.downloader.extract_info(
                player.playlist.loop,
                song_url,
                download=False,
                process=True,    
                on_error=lambda e: asyncio.ensure_future(
                    self.safe_send_message(channel, "```\n%s\n```" % e, expire_in=120), loop=self.loop),
                retry_on_error=True
            )

            if not info:
                raise exceptions.CommandError(
                    "Error extracting info from search string, youtubedl returned no data.  "
                    "You may need to restart the bot if this continues to happen.", expire_in=30
                )

            if not all(info.get('entries', [])):

                return

            song_url = info['entries'][0]['webpage_url']
            info = await self.downloader.extract_info(player.playlist.loop, song_url, download=False, process=False)


        if permissions.max_song_length and info.get('duration', 0) > permissions.max_song_length:
            raise exceptions.PermissionsError(
                "Song duration exceeds limit (%s > %s)" % (info['duration'], permissions.max_song_length),
                expire_in=30
            )

        try:
            entry, position = await player.playlist.add_left(song_url, channel=channel, author=author)

        except exceptions.WrongEntryTypeError as e:
            if e.use_url == song_url:
                print("[Warning] Determined incorrect entry type, but suggested url is the same.  Help.")

            if self.config.debug_mode:
                print("[Info] Assumed url \"%s\" was a single entry, was actually a playlist" % song_url)
                print("[Info] Using \"%s\" instead" % e.use_url)

            return await self.cmd_play(player, channel, author, permissions, leftover_args, e.use_url)

        reply_text = "Enqueued **%s** to be played. Position in queue: %s"
        btext = entry.title

        position = 'Up next!'
        reply_text %= (btext, position)

        if not test.addUser(author.name,1):
            test.addScore(author.name)
        
        append_write = ''
        logstr = '{} {} requested : {}\n'.format(str(datetime.now()), author.name, entry.title)
        print(logstr)
        if os.path.exists('KrBotLog.txt'):
            append_write = 'a' 
        else:
            append_write = 'w' 

        log = open('KrBotLog.txt',append_write,encoding="utf8")
        log.write(logstr)
        log.close()
        
        return Response(reply_text, delete_after=30)

    async def cmd_nudge(self, user_mentions, author):
        """
        Usage:
            {command_prefix}nudge <user>
            
        Asks KrBot to send direct message to user to get his attention
        """
        
        usr = user_mentions[0]
        if usr.name.lower() == 'krbot':
            return
        return await discord.Client.send_message(self, destination=usr, content="<@{}> requests your attention in Chill Heaven".format(author.id), tts=True)
        
        
    async def cmd_remove(self, message, player, index):
        """
        Usage:
            {command_prefix}remove [number]
         
        Removes a song from the queue at the given position, where the position is a number from {command_prefix}queue.
        """
 
        if not player.playlist.entries:
            raise exceptions.CommandError("There are no songs queued.", expire_in=20)
 
        try:
            index = int(index)
 
        except ValueError:
            raise exceptions.CommandError('{} is not a valid number.'.format(index), expire_in=20)
 
        if 0 < index <= len(player.playlist.entries):
            try:
                song_title = player.playlist.entries[index-1].title
                player.playlist.remove_entry((index)-1)
 
            except IndexError:
                raise exceptions.CommandError("Something went wrong while the song was being removed. Try again with a new position from `" + self.config.command_prefix + "queue`", expire_in=20)
 
            return Response("\N{CHECK MARK} removed **" + song_title + "**", delete_after=20)
 
        else:
            raise exceptions.CommandError("You can't remove the current song (skip it instead), or a song in a position that doesn't exist.", expire_in=20)
             
    async def cmd_addnp(self, player, author, channel, server, message, lib=None):
        """
        Usage:
            {command_prefix}addnp <optional: target library>
        Adds the song currently playing to the autoplaylist OR if specified, add the song to specified library.
        Ex: !addnp -> adds to auto playlist. !addnp kpop -> adds to kpop library, doesn't update auto playlist
        """
        
        if player._current_entry == None:
            return Response('**Nothing is playing!**', delete_after=30)
        
        if not lib == None and lib.lower() in self.get_libraries():
            return(await self.cmd_insertlibrary(author, player, player._current_entry.url, lib))
        
        if self.autoplaylist.count(player._current_entry.url) == 0:
            self.autoplaylist.append(player._current_entry.url)
            autoplaylist_file = open(self.config.auto_playlist_file, "a")
            autoplaylist_file.write(player._current_entry.url)
            autoplaylist_file.write("\n")
            autoplaylist_file.close()
            append_write = ''
            logstr = '{} {} requested : {}\n'.format(str(datetime.now()), author.name, player._current_entry.title)
            print(logstr)
            if not test.addUser(author.name,1):
                test.addScore(author.name)
            if os.path.exists('KrBotLog.txt'):
                append_write = 'a' 
            else:
                append_write = 'w' 

            log = open('KrBotLog.txt',append_write,encoding="utf8")
            log.write(logstr)
            log.close()            
            await self.send_message(channel, "Added **%s** to the autoplaylist!" % (player.current_entry.title))
        else:
            await self.send_message(channel, "**%s** is already in the autoplaylist!" % (player.current_entry.title))
             
    async def cmd_gif(self, tag=None):
        """
        Usage:
            {command_prefix}gif <tags>
        Gets random GIFS based on tags
        """
        
        if tag == None:
            link = gp.screensaver(tag).url
            split = link.split('/')
            if '-' in split[len(split)-1]:
                temp = split[len(split)-1].split('-')
                res = temp[len(temp)-1]
            else:
                res = split[len(split)-1]
            return Response('http://media.giphy.com/media/' + res + '/giphy.gif', reply = False, delete_after=40)            
        
        else:
            temp = gp.screensaver(tag)
            if temp is None:
                return Response('**Can\'t find that gif, try another keyword.**', reply = True, delete_after=30)
            link = temp.url
            split = link.split('/')
            if '-' in split[len(split)-1]:
                temp = split[len(split)-1].split('-')
                res = temp[len(temp)-1]
            else:
                res = split[len(split)-1]
            return Response('http://media.giphy.com/media/' + res + '/giphy.gif', reply = False, delete_after=40)

    async def cmd_rip(self):
        """
        Usage:
            {command_prefix}rip
        See for yourself. God Bless You My Friend
        """
        return Response('https://ripme.xyz', reply = False, delete_after=50)

    def getXML(self):
        with urllib.request.urlopen('http://content.warframe.com/dynamic/rss.php') as response:
            return response.read()

    async def cmd_invasions(self):
        """
        Usage:
            {command_prefix}invasions
        Gets the current active invasions and Outbreaks
        """
        return Response(str(wf.getInvasions()), delete_after=80)
    
    
    async def cmd_alerts(self):
        """
        Usage:
            {command_prefix}alerts
        Gets the current alerts
        """
        return Response(str(wf.getAlerts()), delete_after=60)

    async def cmd_status(self):
        """
        Usage:
            {command_prefix}status
        Gets the bot's tracking status
        """
        if not self.tracking:
            return Response('**Tracking Status**: Not Tracking',delete_after=40)
        else:
            return Response('**Tracking Status**: Tracking {}'.format(self.tracked_item),delete_after=40)

    async def cmd_interval(self, val=None):
        """
        Usage:
            {command_prefix}interval <optional : value in minutes>
        Displays current interval, or sets interval if argument is set.
        """
        if val is not None and not val.isdigit():
            return Response('**Invalid arguments provided. Use numbers!**', delete_after = 30)
        if val == None:
            return Response('**Current alert check interval: {} minutes**'.format(self.interval), delete_after = 30)
        else:
            self.interval = int(val)
            if self.sched is not None:
                self.sched.get_jobs()[0].reschedule('interval',minutes=self.interval)
            return Response('**Current alert check interval set to {} minutes**'.format(val), delete_after = 30)
         
    async def checkloop(self, channel):
        if self.tracking:
            if self.tracking:
                soup = BeautifulSoup(self.getXML(),"html.parser")
                for item in soup.findAll('item'):
                    if item.author.string.lower() == 'alert':
                        if len(item.title.string.split('-')) == 4:
                            splitted = item.title.string.split('-')
                            location = splitted[2]
                            time = (datetime.strptime(item.findChildren()[6].string, "%a, %d %b %Y %H:%M:%S %z").replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None) - datetime.utcnow().replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None)).seconds
                            rewards = [x.lower() for x in splitted[0:2]]
                            if any(self.tracked_item in x for x in rewards):
                                await self.safe_send_message(channel, '**{} found in current active alerts, time left: {}**'.format(self.tracked_item.capitalize(), self.pretty_time_delta(time)))                        
                        else:
                            splitted = item.title.string.split('-')
                            location = splitted[1]
                            time = (datetime.strptime(item.findChildren()[6].string, "%a, %d %b %Y %H:%M:%S %z").replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None) - datetime.utcnow().replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None)).seconds
                            rewards = splitted[0].lower()
                            if self.tracked_item in rewards:
                                await self.safe_send_message(channel, '**{} found in current active alerts, time left: {}**'.format(self.tracked_item.capitalize(), self.pretty_time_delta(time)))                           
        else:
            print('passed, not tracking')
    
    async def cmd_track(self, item, channel):
        """
        Usage:
            {command_prefix}track <item>
        Gets the current alerts
        """
        if item.lower() == 'off':
            self.tracking = False
            self.tracked_item = ''
            if not self.sched == None:
                self.sched.remove_job('trackloop')
                self.sched.shutdown(False)
                self.sched = None
            return Response('**Stopped Tracking**', reply = True, delete_after=40)
        self.tracking = True
        self.tracked_item = item
        if self.sched == None:
            self.sched = bs()
            self.sched.add_job(self.checkloop, 'interval', [channel], minutes=self.interval, id='trackloop')
            self.sched.start()
        return Response('**Started Tracking {}**'.format(self.tracked_item), reply = True, delete_after = 40)

    def pretty_time_delta(self, seconds):
        sign_string = '-' if seconds < 0 else ''
        seconds = abs(int(seconds))
        days, seconds = divmod(seconds, 86400)
        hours, seconds = divmod(seconds, 3600)
        minutes, seconds = divmod(seconds, 60)
        if days > 0:
            return '%s%dd %dh %dm %ds' % (sign_string, days, hours, minutes, seconds)
        elif hours > 0:
            return '%s%dh %dm %ds' % (sign_string, hours, minutes, seconds)
        elif minutes > 0:
            return '%s%dm %ds' % (sign_string, minutes, seconds)
        else:
            return '%s%ds' % (sign_string, seconds)        
            
    async def cmd_check(self):
        """
        Usage:
            {command_prefix}check
        Checks if current tracked item exists in current active alerts
        """
        if self.tracking:
            soup = BeautifulSoup(self.getXML(),"html.parser")
            for item in soup.findAll('item'):
                if item.author.string.lower() == 'alert':
                    if len(item.title.string.split('-')) == 4:
                        splitted = item.title.string.split('-')
                        location = splitted[2]
                        time = (datetime.strptime(item.findChildren()[6].string, "%a, %d %b %Y %H:%M:%S %z").replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None) - datetime.utcnow().replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None)).seconds
                        rewards = [x.lower() for x in splitted[0:2]]
                        if any(self.tracked_item in x for x in rewards):
                            return Response('**{} found in current active alerts, time left: {}**'.format(self.tracked_item.capitalize(), self.pretty_time_delta(time)), delete_after = 40)                        
                    else:
                        splitted = item.title.string.split('-')
                        location = splitted[1]
                        time = (datetime.strptime(item.findChildren()[6].string, "%a, %d %b %Y %H:%M:%S %z").replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None) - datetime.utcnow().replace(tzinfo=tz.gettz('UTC')).astimezone(tz=None)).seconds
                        rewards = splitted[0].lower()
                        if self.tracked_item in rewards:
                            return Response('**{} found in current active alerts, time left: {}**'.format(self.tracked_item.capitalize(), self.pretty_time_delta(time)), delete_after = 40)                        
            return Response('**{} not found in current active alerts**'.format(self.tracked_item.capitalize()), delete_after = 40)    
        else:
            return Response('**Bot currently not tracking anything**', delete_after = 40) 

    def getRandom(self, type):
        path = 'C:/Users/Juven/MusicBot/libs/' + type.lower() + '.txt'

        file = open(path,'r')
        listsongs = file.readlines()

        selected = random.randint(1,len(listsongs)-1)

        print('selected: {}, link : {}'.format(selected,listsongs[selected]))
        return listsongs[selected]    

    def get_libraries(self):
        filelist = list()
        for file in os.listdir("libs"):
           if file.endswith(".txt"):
              filelist.append(file.split('.')[0])
        return filelist
        
    async def cmd_libraries(self):
        """
        Usage:
            {command_prefix}libraries
        Prints list of currently known libraries
        """
        libs = ', '.join(self.get_libraries())
        return Response('**Currently known libraries are {}**'.format(libs), reply = True, delete_after=60)
            
        
    async def cmd_random(self, listtype, player, channel, author, permissions, leftover_args):
        """
        Usage:
            {command_prefix}random <library name>
        Gets a random song from specified library. Type !libraries to get list of known libraries
        """
        if listtype.lower() in self.get_libraries():
            return(await self.cmd_playnext(player, channel, author, permissions, leftover_args,self.getRandom(listtype)))
        else:
            return Response('**That library does not exists! type !libraries to get list of known libraries**', reply = True, delete_after = 60)
        
    async def cmd_deletelibrary(self, libname, author):
        """
        Usage:
            {command_prefix}deletelibrary <library name>
        Creates a new songs library with given name.
        """   
        path = 'C:/Users/Juven/MusicBot/libs/' + libname.lower() + '.txt'
        if not os.path.exists(path):
            return Response('**That library does not exists!**', reply = True, delete_after=35)
        else:
            os.remove(path)
            append_write = ''
            if os.path.exists('KrBotLog.txt'):
                append_write = 'a' 
            else:
                append_write = 'w' 
            log = open('KrBotLog.txt',append_write,encoding="utf8")
            logstr = '{} {} Deleted {} library\n'.format(str(datetime.now()), author.name, libname)               
            log.write(logstr)
            log.close()
            return Response('**Library {} deleted**'.format(libname), reply = False, delete_after = 35)
            
    async def cmd_createlibrary(self, libname, author):
        """
        Usage:
            {command_prefix}createlibrary <library name>
        Creates a new songs library with given name.
        """
        path = 'C:/Users/Juven/MusicBot/libs/' + libname.lower() + '.txt'
        if os.path.exists(path):
            return Response('**That library already exists!**', reply = True, delete_after=35)
        else:
            file = open(path,'w')
            append_write = ''
            if os.path.exists('KrBotLog.txt'):
                append_write = 'a' 
            else:
                append_write = 'w' 
            log = open('KrBotLog.txt',append_write,encoding="utf8")
            logstr = '{} {} Created {} library\n'.format(str(datetime.now()), author.name, libname)               
            log.write(logstr)
            log.close()
            return Response('**Library {} created**'.format(libname), reply = False, delete_after = 35)
            
    def tail(self, f, lines):
        return "\n".join(f.read().split("\n")[-lines:])
        
    async def cmd_printlog(self, type, count, channel):
        """
        Usage:
            {command_prefix}printlog <Type (chat/song/command)> <N>
        Prints last N lines from the log.
        Prints to channel if N <= 20, else sends text to channel.
        """
        if not type.lower() in ['chat','song','command']:
            return Response('**Your request is invalid, see help.**', reply = True, delete_after=30)
        if not count.isdigit():
            return Response('**Argument must be a number, {} provided**'.format(count),delete_after=30)
        if type.lower() == 'song':
            f = open('KrBotLog.txt',encoding="utf8")
        elif type.lower() == 'chat':
            f = open('ChatLog.txt', encoding="utf8")
        else:
            f = open('CommandsLog.txt', encoding="utf8")
        if int(count) <= 20:
            res = '```'
            res += self.tail(f, int(count) + 1)
            res += '```'
            return Response(res, reply = False, delete_after = 40)
        else:
            with BytesIO() as fcontent:
                fcontent.write('\r\n'.join(self.tail(f,int(count)+1).split('\n')).encode('utf8'))

                fcontent.seek(0)
                await self.send_file(channel, fcontent, filename='KrBotLog.txt', content="Here's the last %d lines of the log" % int(count))

            return Response(":mailbox_with_mail:", delete_after=20)
        
    async def cmd_putlist(self, listtype, player, channel, author, permissions, leftover_args):
        """
        Usage:
            {command_prefix}putlist <list type>
        Puts a list from spcific type to playlist. Type !libraries to get list of known libraries
        """
        print('cmd_putlist : {}'.format(listtype))
        if listtype.lower() in self.get_libraries():
            f = open('libs/' + listtype + '.txt',encoding="utf8")
            entries = f.readlines()
            busymsg = await self.safe_send_message(
                channel, "Processing %s songs..." % len(entries))  
            await self.send_typing(channel)
            for x in entries:
                try:
                    currResult = await player.playlist.add_entry(x, channel=channel, author=author)
                except Exception as e:
                    continue
            await self.safe_delete_message(busymsg)
            return Response('**Added {} songs to the playlist'.format(len(entries)), reply = True)
        else:
            return Response('**That library does not exists! type !libraries to get list of known libraries**', reply = True, delete_after = 60)

    async def cmd_repeat(self, player, channel, author, permissions, leftover_args):
        """
        Usage:
            {command_prefix}repeat
        Takes the current song and puts it into the next song
        """
        
        return(await self.cmd_playnext(player, channel, author, permissions, leftover_args, player.current_entry.url))
    
    async def cmd_owrank(self, id):
        """
        Usage:
            {command_prefix}owrank <id>
        Gives the overwatch rank for the specified player. Only US region is supported at the moment.
        ID example: SamGyeopSal-11160
        It is important to put id, then a dash, then the number, otherwise it won't work.
        """
        req = urllib.request.Request('https://owapi.net/api/v3/u/'+id+'/blob', headers={'User-Agent': 'Mozilla/5.0'})
        print('https://owapi.net/api/v3/u/'+id+'/blob')
        try:
            page = urllib.request.urlopen(req)
        except:
            return Response('**The user {} is not found**'.format(id.replace('-','#')),reply = True, delete_after=60)
        opened = page.read().decode("utf-8")
        res = json.loads(opened)
        if res['us'] == None:
            return Response('**No record for {} in US region**'.format(id))
        return Response('**Rank for user {} is {}**'.format(id.replace('-','#'),res['us']['stats']['competitive']['overall_stats']['comprank']),reply = True, delete_after=60)

    async def cmd_lolprintuser(self, channel, id):
        """
        Usage:
            {command_prefix}lolprintuser <id>
        Prints out the summoner for specified ID. Currently only works for NA region.
        """
        return await self.safe_send_message(channel, 'not implemented yet')

    def getStats(self):
        userslist = [x.name for x in ps.users()]
        uptime = ut._uptime_windows()
        m, s = divmod(uptime, 60)
        h, m = divmod(m, 60)
        cpuusage = '{}%'.format(ps.virtual_memory().percent)
        timestr = '{:.0f} H {:.0f} M {:.0f} S'.format(h,m,s)
        stats = {'cpu' : cpuusage, 'users' : userslist, 'uptime' :timestr}
        return stats

    @owner_only
    async def cmd_serverstats(self, author, channel):
        stats = self.getStats()
        strres = '**```'
        strres += 'Server Status\n'
        strres += 'CPU usage           : {}\n'.format(stats['cpu'])
        strres += 'Logged in users     : {}\n'.format(' '.join(stats['users']))
        strres += 'Active server uptime: {}'.format(stats['uptime'])
        strres += '```**'
        return(await self.safe_send_message(channel, strres))

    @owner_only
    async def cmd_ban(self, user_mentions, channel):
        if not user_mentions:
            return Response('**No user(s) mentioned**', reply = True, delete_after = 20)
        for user in user_mentions:
            if user.id == self.config.owner_id:
                return Response('**Cannot ban server owner**', reply = True, delete_after = 20)
            self.banned.append(user.id)
            await self.safe_send_message(channel, '{} has been banned and cannot access commands'.format(user.name))
        return await self.updateBanList()

    @owner_only
    async def cmd_mute(self, user_mentions, channel):
        if not user_mentions:
            return Response('**No user(s) mentioned**', reply = True, delete_after = 20)
        for user in user_mentions:
            if user.id == self.config.owner_id:
                return Response('**Cannot ban server owner**', reply = True, delete_after = 20)
            self.muted.append(user.id)
            await self.safe_send_message(channel, '{} has been muted and cannot talk'.format(user.name))
        
    @owner_only
    async def cmd_unmute(self,user_mentions, channel):
        if not user_mentions:
            return Response('**No user(s) mentioned**', reply = True, delete_after = 20)
        for user in user_mentions:
            if(user.id in self.muted):
                self.muted.remove(user.id)
                await self.safe_send_message(channel, '{} has been cleared'.format(user.name))
        
    async def updateBanList(self):
        if os.path.exists('config/banlist.txt'):
            os.remove('config/banlist.txt')
        banlist = open('config/banlist.txt','w',encoding="utf8")
        for user in self.banned:
            if user.strip():
                banlist.write(user)
            
    @owner_only
    async def cmd_banlist(self):
        l = []
        for x in self.banned:
            l.append('<@' + x + '>')
        return Response('Banned users: {}'.format(str.join(', ', l)),reply = False, delete_after = 20)
    
    @owner_only
    async def cmd_resetbans(self):
        self.banned = deque()
        await self.updateBanList()
        return Response('**All users has been unbanned**', reply = True, delete_after = 20)
    
    @owner_only
    async def cmd_unban(self,user_mentions, channel):
        if not user_mentions:
            return Response('**No user(s) mentiond**', reply = True, delete_after = 20)
        for user in user_mentions:
            if(user.id in self.banned):
                self.banned.remove(user.id)
                await self.safe_send_message(channel, '{} has been unbanned'.format(user.name))
        return await self.updateBanList()
        
    
        
if __name__ == '__main__':
    bot = MusicBot()
    bot.run()
