discord.py[voice] ~= 0.12.0
youtube_dl
pip
cffi==1.6.0; sys_platform == 'win32'
selenium
python-dateutil
aiohttp
beautifulsoup4
giphypop
apscheduler
psutil
uptime
riotwatcher